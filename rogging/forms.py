from django import forms
from django.core.exceptions import ValidationError
import models

class LoginForm(forms.Form):
	username = forms.CharField(max_length=200, label='Username:')
	password = forms.CharField(widget=forms.PasswordInput, label='Password:')

def usernameValidator(value):
	for c in value:
		if c not in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789_":
			raise ValidationError("You have a character in your username that is not either alphanumeric or an underscore.")
	

class CreateAccountForm(forms.Form):
	username = forms.CharField(max_length=200, label='Username:', validators=[usernameValidator])
	email = forms.CharField(widget=forms.EmailInput, label='Email address')
	password = forms.CharField(widget=forms.PasswordInput, label='Password:')
	confirmPassword = forms.CharField(widget=forms.PasswordInput, label='Confirm Password:')

class ChangePasswordForm(forms.Form):
	currentPassword = forms.CharField(widget=forms.PasswordInput, label='Current Password:')
	newPassword = forms.CharField(widget=forms.PasswordInput, label='New Password:')
	confirmNewPassword = forms.CharField(widget=forms.PasswordInput, label='Confirm New Password:')

def distanceValidator(value):
	try:
		distance = float(value)
	except ValueError:
		raise ValidationError("The distance you entered is not a real number")

def timeRanValidator(value):
	
	# AN AUTOMATON FOR VALIDATING timeRan	
	workingState = 9
	numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
	numbersTime = ['0', '1', '2', '3', '4', '5']
	workingSeconds = ""
	while True:
		if workingState == 9:
			if len(value) == 0:
				break
			elif value[-1] in numbers:
				workingState = 10
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the seconds area of your time. Please correct.")
		elif workingState == 10:
			if len(value) == 0:
				break
			elif value[-1] in numbers:
				workingState = 0
				value = value[:-1]
				continue
			elif value[-1] == ".":
				workingState = 1
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the seconds area of your time. Please correct.")
		elif workingState == 0:
			if len(value) == 0:
				break
			elif value[-1] in numbers:
				workingState = 0
				value = value[:-1]
				continue
			elif value[-1] == ".":
				workingState = 1
				value = value[:-1]
				continue
			elif value[-1] == ":":
				workingState = 4
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the seconds area of your time. Please correct.")			
		elif workingState == 1:
			if len(value) == 0:
				break
			elif value[-1] in numbers:
				workingState = 2
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the seconds area of your time. Please correct.")
		elif workingState == 2:
			if len(value) == 0:
				break
			elif value[-1] in numbersTime:
				workingState = 3
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the seconds area of your time (possibly your ten's position of you seconds is not between 0 and 5 inclusive?)")
		elif workingState == 3:
			if len(value) == 0:
				break;
			elif value[-1] == ":":
				workingState = 4
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the seconds area of your time (are you missing a \":\" there?)")
#		elif workingState == 4:
#			if len(value) == 0:
#				raise ValidationError("Your time seems incomplete; You cannot have nothing before a \":\"")
#			elif value[-1] in numbers:
#				workingState = 5
#				value = value[:-1]
#				continue
#			else:
#				raise ValidationError("Found invalid input in the minutes area of your time (did you use a non-number in the one's place in your time?)")
#		elif workingState == 5:
#			if len(value) == 0:
#				break
#			elif value[-1] in numbersTime:
#				workingState = 6
#				value = value[:-1]
#				continue
#			else:
#				raise ValidationError("Found invalid input in the minutes area of your time (did you use a non-number in the ten's place?)")
		elif workingState == 4:
			if len(value) == 0:
				raise ValidationError("Your time seems incomplete; You cannot have nothing before a \":\"")
			elif value[-1] in numbers:
				workingState = 6
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the minutes area of your time (did you use a non-number in the one's place in your time?)")
		elif workingState == 6:
			if len(value) == 0:
				break
			elif value[-1] in numbers:
				workingState = 6
				value = value[:-1]
				continue
			elif value[-1] == ":":
				workingState = 7
				value = value[:-1]
				continue
			else:
				raise ValidationError("Didn't find a \":\" before the minutes section; please fix") 
#		elif workingState == 6:
#			if len(value) == 0:
#				break
#			elif value[-1] == ":":
#				workingState = 7
#				value = value[:-1]
#				continue
#			else:
#				raise ValidationError("Didn't find a \":\" before the minutes section; please fix") 
		elif workingState == 7:
			if len(value) == 0:
				raise ValidationError("Your time seems incomplete, you cannot have nothing before \":\"")
			elif value[-1] in numbers:
				workingState = 8
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the hours area of your time (did you use a non-number somewhere there?)")
		else:
			if len(value) == 0:
				break
			elif value[0] in numbers:
				workingState = 8
				value = value[:-1]
				continue
			else:
				raise ValidationError("Found invalid input in the hours area of your time. Please fix.")

class LogEntryForm(forms.Form):
	title = forms.CharField(max_length=200, label='Title: ')
	date = forms.DateField(label='Date:')
	distance = forms.CharField(label='Distance:', validators=[distanceValidator])
	entry = forms.CharField(widget=forms.Textarea, label='Entry:', required=False)
	timeRan = forms.CharField(validators=[timeRanValidator], required=False)
	shoe = forms.ChoiceField(widget=forms.Select)
	type = forms.ChoiceField(widget=forms.Select)
	subtype = forms.ChoiceField(widget=forms.Select)
	
	def __init__(self, shoes, *args, **kwargs):
		super(LogEntryForm, self).__init__(*args, **kwargs)
		self.fields['shoe'].choices = shoes
		self.fields['type'].choices = [('Run', 'Run'), ('Bike', 'Bike'), ('Swim', 'Swim'), ('Weights', 'Weights'), ('Rower', 'Rower'), ('Stepper', 'Stepper'), ('Skate', 'Skate'), ('Walk', 'Walk'), ('Ski', 'Ski'), ('Cross Train', 'Cross Train')]
		self.fields['subtype'].choices = [('Select subtype', 'Select subtype'), ('Steady', 'Steady'), ('Tempo', 'Tempo'), ('Repeats', 'Repeats'), ('Intervals', 'Intervals'), ('Hills', 'Hills'), ('Fartlek', 'Fartlek'), ('Pace', 'Pace'), ('RACE', 'RACE'), ('Lower-body', 'Lower-body'), ('Upper-body', 'Upper-body'), ('Circuit', 'Circuit'), ('Laps', 'Laps'), ('Drills', 'Drills')]

class RunViewForm(forms.Form):
	title = forms.CharField(max_length=200, label='Title: ')
	date = forms.DateField(label='Date:')
	distance = forms.CharField(label='Distance:', validators=[distanceValidator])
	entry = forms.CharField(widget=forms.Textarea, label='Entry:', required=False)
	timeRan = forms.CharField(validators=[timeRanValidator], required=False)
	shoe = forms.ChoiceField(widget=forms.Select)
	deleteEntry = forms.BooleanField(required=False, label='Delete Entry?')
	dateAdded = forms.DateField(required=False)	
	type = forms.ChoiceField(widget=forms.Select)
	subtype = forms.ChoiceField(widget=forms.Select)
	
	def __init__(self, shoes, *args, **kwargs):
		super(RunViewForm, self).__init__(*args, **kwargs)
		self.fields['shoe'].choices = shoes
		self.fields['type'].choices = [('Run', 'Run'), ('Bike', 'Bike'), ('Swim', 'Swim'), ('Weights', 'Weights'), ('Rower', 'Rower'), ('Stepper', 'Stepper'), ('Skate', 'Skate'), ('Walk', 'Walk'), ('Ski', 'Ski'), ('Cross Train', 'Cross Train')]
		self.fields['subtype'].choices = [('Select subtype', 'Select subtype'), ('Steady', 'Steady'), ('Tempo', 'Tempo'), ('Repeats', 'Repeats'), ('Intervals', 'Intervals'), ('Hills', 'Hills'), ('Fartlek', 'Fartlek'), ('Pace', 'Pace'), ('RACE', 'RACE'), ('Lower-body', 'Lower-body'), ('Upper-body', 'Upper-body'), ('Circuit', 'Circuit'), ('Laps', 'Laps'), ('Drills', 'Drills')]

#class ChangeShowForm(forms.Form):
#	showWhich = forms.ChoiceField(widget=forms.RadioSelect, choices=(("RU", "Recently Updated"), ("RD", "Recent Date")), label="Organize by")

class MervImportForm(forms.Form):
	mervCSVData = forms.CharField(widget=forms.Textarea, label="Merv CSV data (NO HEADERS, JUST DATA)")
	
def metricNameValidator(value):
	spaceFound = True if " " in value else False
	if spaceFound == True:
		raise ValidationError("Found a space in the name of the metric. Please remove the space.")
		

class SettingsForm(forms.Form):
	shoeName = forms.CharField(max_length=200, label="Shoe Name", required=False)
	metricName = forms.CharField(max_length=200, label="Metric Name", required=False, validators=[metricNameValidator])
	metricDistance = forms.FloatField(label="Metric Distance", required=False)

class WeekPDFForm(forms.Form):
	pdfData = forms.CharField(widget=forms.Textarea, required=True)
	
class PWResetForm(forms.Form):
	username = forms.CharField(max_length=200, required=True)
	
class UsernamesRequestForm(forms.Form):
	emailAddress = forms.CharField(widget=forms.EmailInput, required=True)

class ProfilePicUploadForm(forms.Form):
	picture = forms.ImageField()

class BannerPicUploadForm(forms.Form):
	bannerPicture = forms.ImageField()

def ajaxBooleanValidator(value):
	if (value == "true") or (value == "false"):
		return True
	else:
		return False

def privacyValidator(value):
	if value == "0" or value == "1" or value == "2":
		return True
	else:
		return False

class AccountSetupForm(forms.Form):
	name = forms.CharField(max_length=200, required=False)
	searchUsername = forms.CharField(max_length=5, validators=[ajaxBooleanValidator])
	searchName = forms.CharField(max_length=5, validators=[ajaxBooleanValidator])

class AccountSettingsForm(forms.Form):
	name = forms.CharField(max_length=200, required=False)
	privacy = forms.CharField(max_length=1, validators=[privacyValidator])
	searchUsername = forms.CharField(max_length=5, validators=[ajaxBooleanValidator])
	searchName = forms.CharField(max_length=5, validators=[ajaxBooleanValidator])
	loadUserPageFirst = forms.CharField(max_length=5, validators=[ajaxBooleanValidator])
	email = forms.CharField(widget=forms.EmailInput, required=False)
	password = forms.CharField(widget=forms.PasswordInput)