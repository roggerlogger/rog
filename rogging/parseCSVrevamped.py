def parseLogCSV(csvString):
	
	outerArray = []
	i = 0
	firstNewline = True
	while len(csvString) > 0:
		if firstNewline == False:
			csvString = csvString[2:] #getting rid of the newline
		else:
			firstNewline = False
		#print csvString
		outerArray += [[]]
		for j in xrange(13):
			if j == 0:
				phrase = scanUntilComma(csvString)
				outerArray[-1] +=  [phrase]
				csvString = csvString[len(phrase)+1:]
			else:
				result = scanQuotes(csvString)
				phrase = result[0]
				csvString = result[1]
				outerArray[-1] += [phrase]
	return outerArray

def scanUntilComma(input):
	
	i = 0
	phrase = ""
	while len(input) > 0 and input[0] != ',':				
		phrase += input[i]
		input = input[1:]
	return phrase

def scanQuotes(input):
	workingState = 0
	phrase = ""
	while (True):
		if len(input) > 0:
			#print input
			if workingState == 0:
				if input[0] == ",":
					input = input[1:]
					return (phrase, input)
				else:
					workingState = 1
					input = input[1:]
			
			elif workingState == 1:
				if input[0] == "\"":
					workingState = 2
					input = input[1:]
				else:
					phrase += input[0]
					workingState = 1
					input = input[1:]
			
			elif workingState == 2:
				if input[0] == "\"":
					phrase += "\""
					workingState = 1
					input = input[1:]
				else:
					input = input[1:]
					return (phrase, input)
		else:
			return (phrase, input)		