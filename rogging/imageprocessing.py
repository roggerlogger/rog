from PIL import Image
from StringIO import StringIO
def preparePicture(imageStream, desiredSize, quality=75):
	picture = Image.open(imageStream)
	size = picture.size
	
	pictureRatio = float(size[0])/float(size[1])
	desiredRatio = float(desiredSize[0])/float(desiredSize[1])
	modification = None
	marginTopBottom = 0
	marginSides = 0
	
	if desiredRatio > pictureRatio:
		scale = float(desiredSize[0])/float(size[0])
		modification = picture.resize((int(scale * size[0]), int(scale * size[1])))
		marginTopBottom = ((modification.size[1] - desiredSize[1]) / 2)
		modification = modification.crop((0, marginTopBottom, modification.size[0], modification.size[1] - marginTopBottom))
	else:
		scale = float(desiredSize[1])/float(size[1])
		modification = picture.resize((int(scale * size[0]), int(scale * size[1])))
		marginSides = ((modification.size[0] - desiredSize[0]) / 2)
		modification = modification.crop((marginSides, 0, modification.size[0] - marginSides, modification.size[1]))
	
	modificationStream = StringIO()
	modification.save(modificationStream, 'JPEG', quality=quality, optimize=True, progressive=True)
	
	return modificationStream