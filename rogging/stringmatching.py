import numpy
import bisect
from django.contrib.auth.models import User
import models

### NEEDLEMAN-WUNSCH ALGORITHM ###
def nwMatching(first, second, gap=-1, mismatch=-1, match=3):
	#SETTING UP THE SCORING MATRIX
	m = len(first) + 1
	n = len(second) + 1
	scoringMatrix = numpy.zeros((m, n), dtype=numpy.int16)
	for i in xrange(1, m):
		scoringMatrix[i][0] = gap * i
	for i in xrange(1, n):
		scoringMatrix[0][i] = gap * i
		
	#SCORING
	for i in xrange(1, m):
		for j in xrange(1, n):
			left = scoringMatrix[i][j-1] + gap
			top = scoringMatrix[i-1][j] + gap
			diag = scoringMatrix[i-1][j-1] + (match if first[i-1] == second[j-1] else mismatch)
			
			if left >= top and left >= diag:
				scoringMatrix[i][j] = left
			elif top >= left and top >= diag:
				scoringMatrix[i][j] = top
			else:
				scoringMatrix[i][j] = diag
	
	return scoringMatrix[m-1][n-1]
	


### USER SEARCH ALGORITHM ###
# FROM https://en.wikipedia.org/wiki/Needleman%E2%80%93Wunsch_algorithm
# and
# http://www.cs.utoronto.ca/~brudno/bcb410/lec2notes.pdf
def userSearch(lookingFor, queryingUser, followingWeight=15):
	users = User.objects.filter(is_staff=False)
	sortedUsers = []
	sortedScores = []
	for user in users:
		if not user.roggeelog.private:
			score = None
			if user.roggeelog.searchUsername == True:
				score = nwMatching(lookingFor, user.username)
			if (user.roggeelog.searchName == True) and (user.roggeelog.name != None) and len(user.roggeelog.name) > 0:
				tempScore = nwMatching(lookingFor, user.roggeelog.name)
				if score == None:
					score = tempScore
				elif tempScore > score:
					score = tempScore
			if score != None:
				score += followingWeight if models.Follow.objects.filter(followedLog=user.roggeelog, followerLog=queryingUser.roggeelog).count() > 0 else 0
				position = bisect.bisect(sortedScores, score)
				sortedScores.insert(position, score)
				sortedUsers.insert(position, user)
	print sortedUsers
	sortedUsers.reverse()
	return sortedUsers
			
	
	