from django import template

register = template.Library()

@register.filter
def formatdistance(value):
	numberString = str(value)
	predecimal = ""
	i = 0
	while i < len(numberString) and numberString[i] != ".": # LOGIC NEEDS TO BE IN THIS ORDER TO SHORT-CIRCUIT AT i < len(numberString)
		predecimal += numberString[i]
		i += 1
	i += 1
	postdecimal = ""
	while i < len(numberString) and len(postdecimal) < 2:
		postdecimal += numberString[i]
		i += 1
	#FORMATTING PREDECIMAL TO HAVE COMMAS IN THE RIGHT LOCATIONS
	i = 0
	newPredecimal = ""
	lenPredecimal = len(predecimal)
	lenOfPredecimalZeroIndexed = lenPredecimal - 1

	while i < lenPredecimal:
		if i % 3 == 0:
			if i != 0:
				newPredecimal = "," + newPredecimal
		newPredecimal = predecimal[lenOfPredecimalZeroIndexed - i] + newPredecimal
		i += 1
	
	return newPredecimal + ("." + postdecimal if len(postdecimal) > 0 else "")

@register.filter
def truncateandpreserve(value, numberOfChars):
	return value[0:numberOfChars] + "..." if len(value) > numberOfChars else value

@register.filter
def prettydate(value):
	day = value.day
	dayString = ""
	if day != 11 and day % 10 == 1:
		dayString = str(day) + "st"
	elif day != 12 and day % 10 == 2:
		dayString = str(day) + "nd"
	elif day != 13 and day % 10 == 3:
		dayString = str(day) + "rd"
	else:
		dayString = str(day) + "th"
	return value.strftime("%A, %B " + dayString + ", %Y")
	
@register.filter
def isodate(value):
	return value.isoformat()[:10]

@register.filter
def isodatenodash(value):
	isoformat = value.isoformat()
	return isoformat[0:4] + isoformat[5:7] + isoformat[8:10]

@register.filter
def newlinesToSlashNs(value):
	return value.replace("\n", "\\n").replace("\r", "")
