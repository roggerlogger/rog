from django.http import HttpResponseRedirect
from django.contrib.auth import logout
from datetime import datetime, timedelta
from django.core.urlresolvers import reverse
session_timeout = 172800

class SessionTimeoutMiddleware(object):
	def process_request(self, request):
		if request.user.is_authenticated():
			if request.session.has_key('timeout'):
				timeout = request.session.get('timeout')
				now = datetime.now().isoformat()
				if now > timeout or request.session.get('timeoutLength') != session_timeout:
					logout(request)
					return HttpResponseRedirect(reverse('rogging:loginUser'))
				else:
					newTimeout = datetime.now() + timedelta(seconds=session_timeout)
					request.session['timeout'] = newTimeout.isoformat()
					request.session['timeoutLength'] = session_timeout
					return None
			else:
				newTimeout = datetime.now() + timedelta(seconds=session_timeout)
				request.session['timeout'] = newTimeout.isoformat()
				request.session['timeoutLength'] = session_timeout
				return None
		else:
			return None