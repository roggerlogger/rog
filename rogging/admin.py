from django.contrib import admin
import models


# Register your models here.

admin.site.register(models.LogEntry)
admin.site.register(models.Comment)
admin.site.register(models.Follow)
admin.site.register(models.Shoe)
admin.site.register(models.Tag)
admin.site.register(models.Metric)
admin.site.register(models.PictureCacheURL)
admin.site.register(models.RoggeeLog)
admin.site.register(models.Block)
