from django.shortcuts import render
import forms
from django.contrib.auth import authenticate, login, logout, views
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse, HttpResponseForbidden, HttpResponseBadRequest, HttpResponseNotFound, HttpResponseServerError
from django.contrib.auth.models import User
import models
import datetime, time
from rog.settings import RECAPTCHA_SECRET, RECAPTCHA_SITE_KEY, EMAILER, AWS_PICTURE_ID, AWS_PICTURE_SECRET, PICTURE_BUCKET_NAME, IS_PRODUCTION, STATIC_URL, IS_CONNECTED
import requests, json
from django.shortcuts import get_object_or_404
from django.core.mail import send_mail
import math
import boto3
from boto.s3.connection import S3Connection
from imageprocessing import preparePicture
from django.core.validators import EmailValidator
from cStringIO import StringIO
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django import utils
import django.forms
import parseCSVrevamped
from django.template import loader
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from stringmatching import userSearch
from django.template.context import RequestContext
from time import sleep

pyclamdPresent = True
try:
	import pyclamd
except ImportError:
	pyclamdPresent = False

daysShown = 48
units = "miles"
verbotenUsernames = [	'rogger',
						'accountsetup',
						'search',
						'passwordresetcomplete',
						'passwordresetconfirm',
						'passwordreset',
						'importer',
						'resources',
						'runs',
						'static',
						'media',
						'logger',
						'createaccount',
						'',
						'login',
						'loginsuccess',
						'accountcreated',
						'logoutsuccess',
						'changepassword',
						'changepasswordsuccess',
						'admin'
					]
pictureSignedURLExpiresIn = 10

# Create your views here.
def loginUser(request):
	form = 0

	# IF THE REQUEST IS A POST FROM A FORM AND THE USER IS AUTHENTICATED
	if request.method == 'POST':

		# MAKING FORM WITH THE POSTED DATA
		form = forms.LoginForm(request.POST)

		#GOTTA MAKE SURE THE FORM IS VALID BEFORE CHECKING THE CLEANED DATA
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			loggingInUser = authenticate(username=username, password=password)

			# IF THE LOGIN INFORMATION IS INCORRECT
			if loggingInUser != None:

				# IF THE USER IS ACTIVE, LOG THE USER IN
				# USER IS SAVED IN request.user AFTER LOGIN
				if loggingInUser.is_active:
					login(request, loggingInUser)
					if request.user.roggeelog.loadUserPageFirst == True:
						return HttpResponseRedirect(reverse('rogging:show', kwargs={ 'username': request.user.username }))
					else:
						return HttpResponseRedirect(reverse('rogging:homepage'))
				else:
					form = forms.LoginForm()
					return render(request, 'rogging/logininactive.html', { 'form': form })
			else:
				form = forms.LoginForm()
				return render(request, 'rogging/loginincorrect.html', { 'form': form })
		else:
			return render(request, 'rogging/logininvalid.html', { 'form': form })
	else:
		form = forms.LoginForm()
	
	return render(request, 'rogging/login.html', {'form': form})

def checkRecaptcha(recaptchaResponse):
	data = {'secret': RECAPTCHA_SECRET, 'response': recaptchaResponse }
	response = requests.post("https://www.google.com/recaptcha/api/siteverify", data=data)
	decodedResponse = json.loads(response.text)

	return decodedResponse['success']
	
def createAccount(request):
	form = 0
	
	# IF THE REQUEST IS A POST FROM A FORM
	if request.method == 'POST':
	
		#MAKING FORM WITH THE POSTED DATA
		form = forms.CreateAccountForm(request.POST)

		# CHECKING IF THE FORM IS VALID
		if form.is_valid():
			
			desiredUsername = form.cleaned_data['username']
			desiredPassword = form.cleaned_data['password']
			desiredConfirmPassword = form.cleaned_data['confirmPassword']

			# CHECKING IF THE USERNAME HAS ALREADY BEEN MADE
			found = False
			for user in User.objects.all():
				if user.username.lower() == desiredUsername.lower():
					found = True
			if not found:
				
				# CHECKING IF THE USERNAME IS FORBIDDEN
				if desiredUsername not in verbotenUsernames:
				
					if checkRecaptcha(request.POST['g-recaptcha-response']):
						
						# CHECKING IF THE PASSWORDS MATCH
						if desiredPassword == desiredConfirmPassword:
					
							createdUser = User.objects.create_user(desiredUsername, '', desiredPassword)
							createdUser.email = form.cleaned_data['email']
							createdUser.save()
							createdLog = models.RoggeeLog.objects.create(user=createdUser)
							createdLog.save()
							return HttpResponseRedirect(reverse('rogging:homepage'))

						else:
							form = forms.CreateAccountForm()
							return render(request, 'rogging/createaccountmismatching.html', { 'form': form, 'recaptchaSiteKey': RECAPTCHA_SITE_KEY })
					else:
						form = forms.CreateAccountForm()
						return render(request, 'rogging/createaccountcaptcha.html', { 'form': form, 'recaptchaSiteKey': RECAPTCHA_SITE_KEY })
				else:
					form = forms.CreateAccountForm()
					return render(request, 'rogging/createaccountforbidden.html', { 'form': form, 'recaptchaSiteKey': RECAPTCHA_SITE_KEY })			
			else:
				form = forms.CreateAccountForm()
				return render(request, 'rogging/createaccountalreadyused.html', { 'form': form, 'recaptchaSiteKey': RECAPTCHA_SITE_KEY })
		else:
			form = forms.CreateAccountForm()
			return render(request, 'rogging/createaccountinvalid.html', { 'form': form, 'recaptchaSiteKey': RECAPTCHA_SITE_KEY })
	else:
		form = forms.CreateAccountForm()
	
	return render(request, 'rogging/createaccount.html', { 'form': form, 'recaptchaSiteKey': RECAPTCHA_SITE_KEY })
			
def accountCreated(request):
	return render(request, 'rogging/accountcreated.html', {})

def logoutSuccess(request):
	logout(request)
	return render(request, 'rogging/logoutsuccess.html', {})

def changePassword(request):
	form = 0
	
	# IF THE FORM SUBMITTED DATA VIA POST
	if request.method == 'POST' and request.user.is_authenticated():
		
		form = forms.ChangePasswordForm(request.POST)
		
		# IS FORM VALID
		if form.is_valid():
			
			currentPassword = form.cleaned_data['currentPassword']
			newPassword = form.cleaned_data['newPassword']
			confirmNewPassword = form.cleaned_data['confirmNewPassword']
			
			# CHECK IF THE CURRENT PASSWORD IS CORRECT
			if authenticate(username=request.user.username, password=currentPassword) != None:
				
				# CHECK IF THE NEW PASSWORDS MATCH
				if newPassword == confirmNewPassword:
				
					request.user.set_password(newPassword)
					request.user.save()
					return render(request, 'rogging/changepasswordsuccess.html', {})
				else:
					form = forms.ChangePasswordForm()
					return render(request, 'rogging/changepasswordunmatching.html', { 'form': form })
			else:
				form = forms.ChangePasswordForm()
				return render(request, 'rogging/changepasswordwrongcurrent.html', { 'form': form })
		else:
			form = forms.ChangePasswordForm()
			return render(request, 'rogging/changepasswordinvalid.html', { 'form': form })
	else:
		# GOT TO BE LOGGED IN IN ORDER TO USE THIS FEATURE
		if request.user.is_authenticated():
			form = forms.ChangePasswordForm()
		else:
			return HttpResponseRedirect(reverse('rogging:loginUser'))

	return render(request, 'rogging/changepassword.html', { 'form': form })

def changePasswordSuccess(request):
	return render(request, 'rogging/changepasswordsuccess.html', {})



def getUnitsSorted(respectiveLog):

	# DEFAULT UNITS
	units = ["miles", "mile", "mi.", "mi", "meters", "meter", "m", "kilometers", "kilometer", "km", "k"]
	
	# GETTING METRICS
	metrics = models.Metric.objects.filter(relatedLog=respectiveLog).all()
	
	# ADDING THE METRICS TO THE LIST OF UNITS
	for metric in metrics:
		units += [str(metric.name)]
	
	# SORTING AND REVERSING THE LIST BECAUSE THE UNIT SEARCH ALGORITHM IN JAVASCRIPT REQUIRES THIS ORDER
	units.sort()
	units.reverse()
	
	return str(units)

def getConversions(respectiveLog):
	defaultUnit = "mile";
	kmConv = None
	mileConv = None
	meterConv = None

	# THIS SECTION CONVERTS EVERY UNIT INTO HOW MUCH THE DEFAULT UNIT SPECIFIED ABOVE IT REPRESENTS, 
	# SO IF THE DEFAULT UNIT IS A MILE, THEN A KILOMETER REPRESENTS 0.6125 OF A MILE
	if defaultUnit == "mile":
		kmConv = 0.6215
		mileConv = 1.0
		meterConv = 0.0006215
	else:
		kmConv = 1.0
		mileConv = 1.609
		meterConv = 0.001

	conversions = {"kilometers": kmConv, "kilometer": kmConv, "km": kmConv, "k": kmConv, "miles": mileConv, "mile": mileConv, "mi.": mileConv, "mi": mileConv, "meters": meterConv, "meter": meterConv, "m": meterConv}
	
	metrics = models.Metric.objects.filter(relatedLog=respectiveLog).all()
	
	# ADDING THE CONVERSIONS FOR EACH METRIC
	for metric in metrics:
		conversions[str(metric.name)] = metric.distance
	
	return str(conversions) 

def roggerLogger(request):
	if not request.is_ajax():
		form = 0
		# IF THE METHOD IS A POST AND USER IS LOGGED IN
		
		if request.method == 'POST' and request.user.is_authenticated():
		
			# GETTING USER'S LOG
			respectiveLog = models.RoggeeLog.objects.get(user=request.user)
			
			# GETTING THE UNITS (METRICS) AND THEIR CONVERSIONS
			unitsInSortedOrder = getUnitsSorted(respectiveLog)
			unitConversions = getConversions(respectiveLog)
			date = ''
			form = forms.LogEntryForm(getChoicesForShoes(request.user.username), request.POST)
			
			
			# CHECKING IF THE FORM IS VALID
			if form.is_valid():
				
				# CREATING THE LOG ENTRY
				title = form.cleaned_data['title']
				date = form.cleaned_data['date']
				
				
				entry = form.cleaned_data['entry']
				distance = float(form.cleaned_data['distance'])
				dateTimeCreated = datetime.datetime.now()
				
				timeRan = form.cleaned_data['timeRan']
			
				logEntry = 0
				hoursRan = None
				minutesRan = None
				secondsRan = None
				
				# CHECKING IF THE TIME RAN IS UNSPECIFIED
				if timeRan != '':
					# PARSING THE TIME RAN
					hoursMinutesSeconds = timeRanAutomaton(timeRan)
				
					# IF THE PARSER DISCOVERED AN ERROR IN THE TIME RAN SPECIFIED
					if hoursMinutesSeconds[0] == -1:
						return render(request, 'rogging/loggerinvalid.html', { 'form': form })
	
					# IF THE MINUTES SPECIFIED IS OVER 60, ADD AN HOUR FOR EACH 60 MINUTES FOUND
					hoursRan = hoursMinutesSeconds[0] + math.floor(hoursMinutesSeconds[1]/60)
					
					# GET THE REMAINDER OF MINUTES AFTER HOURS ARE SUBTRACTED
					minutesRan = hoursMinutesSeconds[1] % 60
					
					# SECONDS RAN SHOULD ALREADY BE THE IN THE CORRECT FORMAT
					secondsRan = hoursMinutesSeconds[2]
				
				# GETTING THE SHOES FOR THE USER
				shoes = models.Shoe.objects.filter(name=form.cleaned_data['shoe'], relatedLog=respectiveLog).all()
				shoe = None
				
				# GETTING THE TYPE
				type = form.cleaned_data['type'];
				
				# GETTING THE SUBTYPE
				subtype = form.cleaned_data['subtype']
				if subtype == "Select subtype":
					subtype = None
				
				
				# OK CONFUSED ON THIS ONE...NEED TO INVESTIGATE
				if shoes.count() > 0:
					shoe = shoes[0]
				
				
				# MAKE THE LOG ENTRY
				logEntry = models.LogEntry.objects.create(relatedLog=respectiveLog,
														title=title,
														date=date,
														entry=entry,
														distance=distance,
														dateTimeCreated=dateTimeCreated,
														hoursRan=hoursRan,
														minutesRan=minutesRan,
														secondsRan=secondsRan,
														shoe=shoe,
														kind=type,
														mervSubtype=subtype)
	
				# SAVING THE LOG ENTRY
				logEntry.save()
				
				# GET THE TAGS AND TAG USERS
				tagEntry(logEntry)

				return HttpResponseRedirect(reverse('rogging:show', kwargs={ 'username': request.user.username }))

			else:
				#form.fields['shoe'] = django.forms.ChoiceField(widget=django.forms.Select, choices=getChoicesForShoes(request.user.username))
				return render(request, 'rogging/loggerinvalid.html', { 'form': form, 'date': date, 'units': unitsInSortedOrder, 'unitConversions': unitConversions })
		else:
			# MAKE SURE THE USER IS AUTHENTICATED BEFORE ALLOWING THE USER TO WRITE A LOG ENTRY
			if request.user.is_authenticated():
				
				# GETTING THE USER'S LOG
				respectiveLog = models.RoggeeLog.objects.get(user=request.user)
				
				# GETTING THE UNITS (METRICS) AND THEIR CONVERSIONS
				unitsInSortedOrder = getUnitsSorted(respectiveLog)
				unitConversions = getConversions(respectiveLog)
				date = ''
				
				# CHECKING TO SEE IF THIS LOG HAS A PRELOADED DATE
				if request.session.has_key('logDate'):
					date = request.session.get('logDate')
					del request.session['logDate']
					
				# INITIATE A LOG ENTRY FORM WITH THE USER'S SHOES
				form = forms.LogEntryForm(getChoicesForShoes(request.user.username))
				respectiveLog = models.RoggeeLog.objects.get(user=request.user)
				
				# GETTING LAST SHOE
				latestEntries = models.LogEntry.objects.filter(relatedLog=respectiveLog).order_by('-dateTimeCreated')
				shoe = None
				if latestEntries.count() > 0:
					shoe = latestEntries[0].shoe
				
				
				form = forms.LogEntryForm(getChoicesForShoes(request.user.username), initial={'shoe': shoe.name if shoe != None else "Select shoe" })
				if shoe != None:
					form.data['shoe'] = shoe.name
				
				return render(request, 'rogging/logger.html', { 'form': form, 'date': date, 'units': unitsInSortedOrder, 'unitConversions': unitConversions })
			else:
				return HttpResponseRedirect(reverse('rogging:loginUser'))

		return render(request, 'rogging/logger.html', { 'form': form, 'date': date, 'units': unitsInSortedOrder, 'unitConversions': unitConversions })

	else:
		theType = request.POST['type'].encode('utf-8')
		if theType == "date":
			
			#SETTING THE DATE IN THE SESSION TO BE RETREIVED BY THE NEXT PAGE
			date = request.POST['date'].encode('utf-8')
			request.session['logDate'] = date
			return render(request, 'rogging/null.html', {})
		else:
			return render(request, 'null.html', {})

def timeRanAutomaton(timeRan):
	# AN AUTOMATON FOR PARSING THE DURATION OF A WORKOUT
	# STATES ARE SELF-EXPLANATORY
	workingState = 9
	numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
	numbersTime = ['0', '1', '2', '3', '4', '5']
	workingHours = ""
	workingMinutes = ""
	workingSeconds = ""
	hoursRan = 0
	minutesRan = 0
	secondsRan = 0
	while True:
		if workingState == 9:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbers:
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 10
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		elif workingState == 10:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbers:
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 0
				timeRan = timeRan[:-1]
				continue
			elif timeRan[-1] == ".":
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 1
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		if workingState == 0:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbers:
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 0
				timeRan = timeRan[:-1]
				continue
			elif timeRan[-1] == ".":
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 1
				timeRan = timeRan[:-1]
				continue
			elif timeRan[-1] == ":" and len(workingSeconds) < 3:
				#workingMinutes = workingSeconds
				#workingSeconds = ""
				workingState = 4
				timeRan = timeRan[:-1]
			else:
				return (-1, -1, -1)
		elif workingState == 1:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbers:
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 2
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		elif workingState == 2:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbersTime:
				workingSeconds = timeRan[-1] + workingSeconds
				workingState = 3
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		elif workingState == 3:
			if len(timeRan) == 0:
				break;
			elif timeRan[-1] == ":":
				workingState = 4
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		elif workingState == 4:
			if timeRan[-1] in numbers:
				workingState = 6
				workingMinutes = timeRan[-1] + workingMinutes
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		elif workingState == 6:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbers:
				workingState = 6
				workingMinutes = timeRan[-1] + workingMinutes
				timeRan = timeRan[:-1]
				continue
			elif timeRan[-1] == ":":
				workingState = 7
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		elif workingState == 7:
			if timeRan[-1] in numbers:
				workingHours = timeRan[-1] + workingHours
				workingState = 8
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
		else:
			if len(timeRan) == 0:
				break
			elif timeRan[-1] in numbers:
				workingHours = timeRan[-1] + workingHours
				workingState = 8
				timeRan = timeRan[:-1]
				continue
			else:
				return (-1, -1, -1)
	
	if len(workingSeconds) > 0:
		secondsRan = float(workingSeconds)
	if len(workingMinutes) > 0:
		minutesRan = float(workingMinutes)
	if len(workingHours) > 0:
		hoursRan = float(workingHours)			
			
	return (hoursRan, minutesRan, secondsRan)			

def getAWSInfo():
	session = None
	connection = None
	client = None
	s3 = None
	bucket = None
	boto2bucket = None
	if IS_CONNECTED:
		try:
			session = boto3.session.Session(AWS_PICTURE_ID, AWS_PICTURE_SECRET)
			connection = S3Connection(AWS_PICTURE_ID, AWS_PICTURE_SECRET)
			client = session.client('s3')
			s3 = session.resource('s3')
			bucket = s3.Bucket(PICTURE_BUCKET_NAME)
			boto2bucket = connection.get_bucket(PICTURE_BUCKET_NAME)
			
			return {
						'session': session,
						'connection': connection,
						'client': client,
						's3': s3,
						'bucket': bucket,
						'boto2bucket': boto2bucket
					}
		except:
			return None
	else:
		return None
	
	
	

def awsImageRetrieval(userOfPage, resource, awsInfo):
	overallStartTime = time.time()
	respectiveLog = get_object_or_404(models.RoggeeLog, user=userOfPage)
	signedProfPicUrl = ""
	session = None
	connection = None
	client = None
	s3 = None
	bucket = None
	boto2bucket = None
	
	ending = ''
	if resource == 'profile':
		ending = 'profpic.jpg'
	elif resource == 'banner':
		ending = 'banner.jpg'
	else:
		ending = 'profthumb.jpg'
	
	if IS_CONNECTED:
		try:
			session = awsInfo['session']
			connection = awsInfo['connection']
			client = awsInfo['client']
			s3 = awsInfo['s3']
			bucket = awsInfo['bucket']
			boto2bucket = awsInfo['boto2bucket']
			signedPicUrl = ""
			startTime = time.time()
			goodToGo = False
			if (resource == 'profile' or resource == 'thumbnail') and respectiveLog.hasProfilePicture == True:
				goodToGo = True
			elif resource == 'banner' and respectiveLog.hasBannerPicture:
				goodToGo = True
			
			if goodToGo:
				urls = models.PictureCacheURL.objects.filter(relatedLog=respectiveLog, forResource=resource)
				if urls.count() < 1:
					signedPicUrl = client.generate_presigned_url('get_object', Params={ 'Bucket': PICTURE_BUCKET_NAME, 'Key': str(userOfPage.id) + ending }, ExpiresIn=pictureSignedURLExpiresIn)
					url = models.PictureCacheURL.objects.create(relatedLog=respectiveLog, forResource=resource, url=signedPicUrl)
					url.save()
				else:
					if urls[0].stillValid():
						signedPicUrl = urls[0].url
						if urls.count() > 1:
							urlsToBeAxed = urls[1:]
							for url in urlsToBeAxed:
								url.delete()
					else:
						for url in urls:
							url.delete()
						signedPicUrl = client.generate_presigned_url('get_object', Params={ 'Bucket': PICTURE_BUCKET_NAME, 'Key': str(userOfPage.id) + ending }, ExpiresIn=pictureSignedURLExpiresIn)
						url = models.PictureCacheURL.objects.create(relatedLog=respectiveLog, forResource=resource, url=signedPicUrl)
						url.save()
			else:
				if resource == 'thumbnail':
					signedPicUrl = STATIC_URL + "boatthumb.png"
				else:
					signedPicUrl = STATIC_URL + "boat.png"
		except:
			print "failed in aws"
			if resource == 'thumbnail':
				signedPicUrl = STATIC_URL + "boatthumb.png"
			else:
				signedPicUrl = STATIC_URL + "boat.png"
	else:
		if resource == 'thumbnail':
			signedPicUrl = STATIC_URL + "boatthumb.png"
		else:
			signedPicUrl = STATIC_URL + "boat.png"
	return signedPicUrl

def uploadPicture(user, originalPictureStream, kind):
	clamdProblem = False
	if pyclamdPresent:
		pyclamdSocket = None
		try:
			pyclamdSocket = pyclamd.ClamdAgnostic()
		except:
			if IS_PRODUCTION:
				sendEmail("Cannot connect to Clamd server", "Contact " + user.email + " about username \"" + user.username + "\"", ["ben@rogger.co"])
				pyclamdSocket = None
				clamdProblem = True
		if not clamdProblem:
			if pyclamdSocket != None and pyclamdSocket.ping() == True:
				if pyclamdSocket.scan_stream(originalPictureStream.read()) != None:
					return "virus"
			else:
				if IS_PRODUCTION:
					sendEmail("Clamd cannot be pinged", "Contact " + user.email + " about username \"" + user.username + "\"", ["ben@rogger.co"])
					clamdProblem = True
	else:
		if IS_PRODUCTION:
			sendEmail("pyClamd missing on Rogger sever", "Contact " + user.email + " about username \"" + user.username + "\"", ["ben@rogger.co"])
			clamdProblem = True

	if not clamdProblem:
		print "into the picture processing"
		if kind == "profpic":
			pictureStream = preparePicture(originalPictureStream, (500, 500))
		else:
			pictureStream = preparePicture(originalPictureStream, (1560, 1000))
		profileThumbnailStream = None
		if kind == "profpic":
			profileThumbnailStream = preparePicture(originalPictureStream, (100, 100))
		session = None
		s3 = None
		bucket = None
		if IS_CONNECTED:
			session = boto3.session.Session(AWS_PICTURE_ID, AWS_PICTURE_SECRET)
			s3 = session.resource('s3')
			bucket = s3.Bucket(PICTURE_BUCKET_NAME)
			bucket.put_object(Key=str(user.id)+kind+'.jpg', Body=pictureStream.getvalue())
			if kind == "profpic":
				bucket.put_object(Key=str(user.id)+'profthumb.jpg', Body=profileThumbnailStream.getvalue())
				user.roggeelog.hasProfilePicture = True
				user.roggeelog.save()
			else:
				user.roggeelog.hasBannerPicture = True
				user.roggeelog.save()
		return "success"
	else:
		return "error"

def show(request, username):
	userOfPage = get_object_or_404(User, username=username)
	# GETTING THE USER'S LOG
	respectiveLog = get_object_or_404(models.RoggeeLog, user=userOfPage)
	renderLevel = respectiveLog.renderLevelForUser(request.user)
	
	if renderLevel == -1:
		return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
	
	permissionToRender = True if renderLevel == 1 else False
	
	blocked = False
	if userOfPage.roggeelog.isBlockedByUser(request.user):
		blocked = True
	
	if permissionToRender:
		weekdays = { 0: 'Mon', 1: 'Tue', 2: 'Wed', 3: 'Thu', 4: 'Fri', 5: 'Sat', 6: 'Sun' }
		form = forms.ProfilePicUploadForm()
		if not request.is_ajax():
			awsInfo = getAWSInfo()	
			signedProfPicUrl = awsImageRetrieval(userOfPage, 'profile', awsInfo)
			signedBannerPicUrl = awsImageRetrieval(userOfPage, 'banner', awsInfo)	
			weeks = []
			today = datetime.datetime.strptime(datetime.date.today().isoformat(), "%Y-%m-%d");
			
			# SHOULD WE RETURN TO THE LAST DATE THAT THEY LOOKED AT?
			lastDateLookedAt = request.COOKIES.get("calendarLast" + str(userOfPage.id))
			if lastDateLookedAt != None:
				today = datetime.datetime.strptime(lastDateLookedAt, "%Y-%m-%d")

			# GET DAYS OF THE WEEK
			days = getDaysInWeekFormat(today)

			# GETTING ALL RUNS FOR A RESPECTIVE LOG, AND FILTERING THEM BY REVERSE DATE ORDER
			allRuns = models.LogEntry.objects.filter(relatedLog=respectiveLog).order_by('-dateTimeCreated').all()

			# GETTING THE FIRST RUN TO INTIALIZE THE JAVASCRIPT THAT WILL FETCH MORE RUNS
			allRuns = allRuns[0:1]	
			j = 0

			# FOR UP TO THE NUMBER OF DAYS SHOWN (6 BECAUSE WE ONLY WANT TO SEND AN INITIALIZING WEEK FOR THE JAVASCRIPT TO USE TO LOAD OTHER WEEKS)
			while j <= daysShown:
				startAt = today - datetime.timedelta(days=21)
				# GET THE DAYS OF THE WEEK
				week = getDaysInWeekFormat(startAt + datetime.timedelta(days=j))

				# KEEPING TRACK OF WEEK'S DISTANCE
				distance = {'Run': 0, 'Bike': 0, 'Swim': 0, 'Weights': 0, 'Rower': 0, 'Stepper': 0, 'Skate': 0, 'Walk': 0, 'Ski': 0, 'Cross Train': 0}

				# FOR EACH DAY, ADD THE DAY'S RUNS' DISTANCES TO THE WEEKLY DISTANCE
				for i in xrange(len(week)):
					runs = models.LogEntry.objects.filter(relatedLog=respectiveLog, date=week[i]).all()
					for run in runs:
						if run.kind in distance:
								distance[run.kind] += run.distanceInDesiredUnits()
						else:
							distance[run.kind] = run.distance
					week[i] = [week[i]] + [runs] + [weekdays[i]] + [distance] 

				# PACKAGING THE DISTANCE WITH THE WEEK
				week = [week] + [distance]

				# ADDING WEEK TO WEEKS
				weeks += [week]
				j += 7	

			# GETTING TOTAL MILES THAT THE USER HAS LOGGED
			totalDistance = 0
			workouts = models.LogEntry.objects.filter(relatedLog=respectiveLog)
			for workout in workouts:
				totalDistance += workout.distanceInDesiredUnits()

			# GETTING POTENTIAL YEARS (from 1850 to 2100)
			years = range(1850, 2101)

			# GETTING THE DEFAULT YEAR AND MONTH FOR THE SELECTS FOR THE CALENDAR VIEW
			defaultYear = today.year
			defaultMonth = today.month

			# GETTING THE FOLLOWS FROM THE USER BROWSING FOLLOWING THE USER OF THE PAGE
			follows = False
			if request.user.is_authenticated():
				followQuerySet = models.Follow.objects.filter(followedLog=userOfPage.roggeelog, followerLog=request.user.roggeelog).all()
				follows = True if followQuerySet.count() > 0 else False

			# MAKING PDF FORM
			pdfForm = forms.WeekPDFForm()

			if request.method == "POST":
				if request.user.is_authenticated() and request.user == userOfPage:
					picture = None
					banner = "banner" in request.POST
					if banner:
						form = forms.BannerPicUploadForm(request.POST, request.FILES)
						if form.is_valid():
							picture = request.FILES['bannerPicture']
					else:
						form = forms.ProfilePicUploadForm(request.POST, request.FILES)
						if form.is_valid():
							picture = request.FILES['picture']
			
					if picture == None or picture.multiple_chunks(20000000):
						return HttpResponseRedirect(reverse('rogging:show', kwargs={ 'username': userOfPage.username }))
					else:
						originalPictureStream = StringIO(picture.read())
		
						result = None
						if banner:
							result = uploadPicture(request.user, originalPictureStream, "banner")
						else:
							result = uploadPicture(request.user, originalPictureStream, "profpic")
			
						if result == "virus":
							return render(request, 'rogging/showprofpicerrorvirusfound.html', { 'units': units, 'useroflog': userOfPage, 'weeks': weeks, 'runs': allRuns, 'earliestDay': weeks[-1][-2][0], 'latestDay': weeks[0][0][0], 'follows': follows, 'totalDistance': totalDistance, 'years': years, 'defaultYear': defaultYear, 'defaultMonth': defaultMonth, 'pdfForm': pdfForm, 'signedProfPicUrl': signedProfPicUrl, 'signedBannerPicUrl': signedBannerPicUrl, "debugging":(not IS_PRODUCTION) })
						elif result == "error":
							return render(request, 'rogging/showprofpicerror.html', { 'units': units, 'useroflog': userOfPage, 'weeks': weeks, 'runs': allRuns, 'earliestDay': weeks[-1][-2][0], 'latestDay': weeks[0][0][0], 'follows': follows, 'totalDistance': totalDistance, 'years': years, 'defaultYear': defaultYear, 'defaultMonth': defaultMonth, 'pdfForm': pdfForm, 'signedProfPicUrl': signedProfPicUrl, 'signedBannerPicUrl': signedBannerPicUrl, "debugging":(not IS_PRODUCTION) })
						else:
							return HttpResponseRedirect(reverse('rogging:show', kwargs={ 'username': userOfPage.username }))
				else:
					return HttpResponseRedirect(reverse('rogging:show', kwargs={ 'username': userOfPage.username }))

			if not respectiveLog.hasBannerPicture:
				signedBannerPicUrl = ""
			
			
			return render(request, 'rogging/show.html', { 'blocked': blocked, 'units': units, 'useroflog': userOfPage, 'weeks': weeks, 'runs': allRuns, 'earliestDay': weeks[-1][-2][0], 'latestDay': weeks[0][0][0], 'follows': follows, 'totalDistance': totalDistance, 'years': years, 'defaultYear': defaultYear, 'defaultMonth': defaultMonth, 'pdfForm': pdfForm, 'signedProfPicUrl': signedProfPicUrl, 'signedBannerPicUrl': signedBannerPicUrl, "debugging":(not IS_PRODUCTION) })
		else:
				ajaxType = request.POST['type'].encode('utf-8')
				# IF WE WANT LATER WEEKS
				if ajaxType == "weeklyLater":
					def possibleResponse():
						# DATE TO WORK WITH (FOR GETTING THE LATER WEEK)
						date = request.POST['date'].encode('utf-8')

						# TAKE THE DATE AND CHANGE ITS FORMAT FOR USE WITH TIME DELTA IN THE NEXT LINE
						currentLatestDate = datetime.datetime.strptime(date, "%Y-%m-%d")

						# GET 7 DAYS LATER
						nextLatestDate = currentLatestDate + datetime.timedelta(days=7)

						# GET THE WEEK FOR THAT LATER DAY
						week = getDaysInWeekFormat(nextLatestDate)

						# KEEPING TRACK OF WEEK'S DISTANCE
						distance = {'Run': 0, 'Bike': 0, 'Swim': 0, 'Weights': 0, 'Rower': 0, 'Stepper': 0, 'Skate': 0, 'Walk': 0, 'Ski': 0, 'Cross Train': 0}
	
						# FOR EACH DAY, ADD THE DAY'S RUNS' DISTANCES TO THE WEEKLY DISTANCE
						for i in xrange(len(week)):
							runs = models.LogEntry.objects.filter(relatedLog=respectiveLog, date=week[i]).all()
							for run in runs:
								if run.kind in distance:
									distance[run.kind] += run.distanceInDesiredUnits()
								else:
									distance[run.kind] = run.distance
							week[i] = [week[i]] + [runs] + [weekdays[i]] + [distance] 
	
						# PACKAGING THE DISTANCE WITH THE WEEK
						week = [week] + [distance]
						return render(request, "rogging/week.html", { 'week': week, 'units': units, 'useroflog': userOfPage })
						
						
					# PROBABLY UNNECESSARY	
					return userOfPage.roggeelog.renderProperResponse(possibleResponse, request)
					
				elif ajaxType == "weeklyEarlier":
					def possibleResponse():
						# DATE TO WORK WITH (FOR GETTING EARLIER WEEK)
						date = request.POST['date'].encode('utf-8')

						# TAKE THE DATE AND CHANGE ITS FORMAT FOR USE WITH THE TIME DELTA IN THE NEXT LINE
						currentEarliestDate = datetime.datetime.strptime(date, "%Y-%m-%d")

						# GET 7 DAYS EARLIER
						nextEarliestDate = currentEarliestDate - datetime.timedelta(days=7)

						# GET WEEK FOR THAT EARLIER DAY
						week = getDaysInWeekFormat(nextEarliestDate)

						# KEEPING TRACK OF WEEK'S DISTANCE
						distance = {'Run': 0, 'Bike': 0, 'Swim': 0, 'Weights': 0, 'Rower': 0, 'Stepper': 0, 'Skate': 0, 'Walk': 0, 'Ski': 0, 'Cross Train': 0}
	
						# FOR EACH DAY, ADD THE DAY'S RUNS' DISTANCES TO THE WEEKLY DISTANCE
						for i in xrange(len(week)):
							runs = models.LogEntry.objects.filter(relatedLog=respectiveLog, date=week[i]).all()
							for run in runs:
								if run.kind in distance:
									distance[run.kind] += run.distanceInDesiredUnits()
								else:
									distance[run.kind] = run.distance
							week[i] = [week[i]] + [runs] + [weekdays[i]] + [distance] 
	
						# PACKAGING THE DISTANCE WITH THE WEEK
						week = [week] + [distance]
						return render(request, "rogging/week.html", { 'week': week, 'units': units, 'useroflog': userOfPage })
					
					return userOfPage.roggeelog.renderProperResponse(possibleResponse, request)
				elif ajaxType == "recentEarlier":
					def possibleResponse():
						# GETTING THE EARLIEST WORKOUT CURRENTLY SHOWN TO THE USER
						laterRunId = request.POST['idOfRun'].encode('utf-8')
						# RETREIVE THAT WORKOUT FROM THE DATABASE
						laterRun = models.LogEntry.objects.get(id=laterRunId)
						# GET ALL THE RUNS
						nextRuns = models.LogEntry.objects.filter(relatedLog=respectiveLog, dateTimeCreated__lt=laterRun.dateTimeCreated).order_by('-dateTimeCreated').all()

						# RETREIVE THE NEXT 30 (OR 31?) WORKOUTS IF THERE ARE MORE OR EXACTLY 30 (OR 31?) WORKOUTS LEFT TO SHOW
						if nextRuns.count() > 30:
							nextRuns = nextRuns[0:30]
						return render(request, 'rogging/recent.html', { 'runs': nextRuns })
					
					return userOfPage.roggeelog.renderProperResponse(possibleResponse, request)
				elif ajaxType == "follow" and request.user.is_authenticated():
					usersLog = get_object_or_404(models.RoggeeLog, user=request.user)
					following = models.Follow.objects.filter(followedLog=respectiveLog, followerLog=usersLog).all()
	
					# CHECKING IF THE FOLLOW ALREADY EXISTS; IF IT DOES, ASSUME THIS AJAX REQUEST IS TO UNFOLLOW
					if following.count() > 0:
						following[0].delete()
					else:
						# NOT UNFOLLOWING
						if userOfPage.roggeelog.pseudoPrivate:
							if userOfPage != request.user:
								following = models.Follow.objects.create(followedLog=respectiveLog, followerLog=request.user.roggeelog, approved=False)
								following.save()
							else:
								following = models.Follow.objects.create(followedLog=respectiveLog, followerLog=request.user.roggeelog, approved=True)
								following.save()
						else:
							following = models.Follow.objects.create(followedLog=respectiveLog, followerLog=request.user.roggeelog)
							following.save()
							
					return render(request, 'rogging/null.html', {})
				elif ajaxType == "weekRequest":
					def possibleResponse():
						isodate = request.POST['date'].encode('utf-8')
						# TAKE THE DATE AND CHANGE ITS FORMAT FOR USE WITH THE TIME DELTA IN THE NEXT LINE
						date = datetime.datetime.strptime(isodate, "%Y-%m-%d")
						# GET WEEK FOR THAT EARLIER DAY
						week = getDaysInWeekFormat(date)

						# KEEPING TRACK OF WEEK'S DISTANCE
						distance = {'Run': 0, 'Bike': 0, 'Swim': 0, 'Weights': 0, 'Rower': 0, 'Stepper': 0, 'Skate': 0, 'Walk': 0, 'Ski': 0, 'Cross Train': 0}
	
						# FOR EACH DAY, ADD THE DAY'S RUNS' DISTANCES TO THE WEEKLY DISTANCE
						for i in xrange(len(week)):
							runs = models.LogEntry.objects.filter(relatedLog=respectiveLog, date=week[i]).all()
							for run in runs:
								if run.kind in distance:
									if run.kind == "Cross Train":
										distance[run.kind] += run.distanceInDesiredUnits()
								else:
									distance[run.kind] = run.distance
							week[i] = [week[i]] + [runs] + [weekdays[i]] + [distance] 
	
						# PACKAGING THE DISTANCE WITH THE WEEK
						week = [week] + [distance]
						return render(request, "rogging/week.html", { 'week': week, 'units': units, 'useroflog': userOfPage })
					
					
					return userOfPage.roggeelog.renderProperResponse(possibleResponse, request)
					
				elif ajaxType == "printWeekRequest":
					if request.user.is_authenticated() and userOfPage.username == request.user.username:
						isodate = request.POST['firstDayOfWeek'].encode('utf-8')
						date = datetime.datetime.strptime(isodate, "%Y-%m-%d")
						week = getWeekOfWorkouts(date, respectiveLog)
						return render(request, "rogging/printweek.html", { 'week': week, 'units': 'miles' })
					else:
						return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))
				elif ajaxType == "generateWeekPDF":
					if request.user.is_authenticated() and userOfPage.username == request.user.username:
						pdfData = request.POST['pdfcontent'].encode('utf-8')
						isodate = request.POST['isodate'].encode('utf-8')
						response = makePDFfromHTML(pdfData)
						fileName = request.user.username + "weekof" + isodate + ".pdf"
						response["Content-Disposition"] = 'inline; filename=' + fileName
						return response
					else:
						return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))
				elif ajaxType == "block":
					userOfPage = User.objects.get(id=int(request.POST['userID']))
					if userOfPage.roggeelog.private:
						return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
					if request.user.is_authenticated():
						if request.user != userOfPage:
							block = models.Block.objects.filter(blockedLog=userOfPage.roggeelog, blockerLog=request.user.roggeelog)
							if block.exists():
								block.delete()
								return JsonResponse({ "blocked": False })
							else:
								block = models.Block.objects.create(blockedLog=userOfPage.roggeelog, blockerLog=request.user.roggeelog)
								block.save()
								return JsonResponse({ "blocked": True })
						else:
							return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))
					else:
						return HttpResponseBadRequest(loader.render_to_string('rogging/400.html', context=RequestContext(request)))
				elif ajaxType == "workoutPreview":
					workoutID = int(request.POST["workoutID"])
					entry = get_object_or_404(models.LogEntry, id=workoutID)
					if entry.relatedLog == userOfPage.roggeelog:
						def possibleResponse():
							return render(request, "rogging/workoutPreview.html", { "workout": entry })
						
						return userOfPage.roggeelog.renderProperResponse(possibleResponse, request)
					else:
						return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
				else:
					return HttpResponseBadRequest(loader.render_to_string('rogging/400.html', context=RequestContext(request)))
				
	else:
		if not request.is_ajax():
			awsInfo = getAWSInfo()	
			signedProfPicUrl = awsImageRetrieval(userOfPage, 'profile', awsInfo)
			signedBannerPicUrl = awsImageRetrieval(userOfPage, 'banner', awsInfo)
			if not respectiveLog.hasBannerPicture:
					signedBannerPicUrl = ""
		
			# GETTING TOTAL MILES THAT THE USER HAS LOGGED
			totalDistance = 0
			workouts = models.LogEntry.objects.filter(relatedLog=respectiveLog)
			for workout in workouts:
				totalDistance += workout.distanceInDesiredUnits()
			
			followRequested = False
			if request.user.is_authenticated():
				followRequested = models.Follow.objects.filter(followedLog=userOfPage.roggeelog, followerLog=request.user.roggeelog).exists()
			
			return render(request, 'rogging/showunapproved.html', { 'blocked': blocked, 'useroflog': userOfPage, 'totalDistance': totalDistance, 'signedProfPicUrl': signedProfPicUrl, 'followRequested': followRequested, 'signedBannerPicUrl': signedBannerPicUrl })
		else:
			ajaxType = request.POST['type'].encode('utf-8')
			if ajaxType == "follow" and request.user.is_authenticated():
				usersLog = get_object_or_404(models.RoggeeLog, user=request.user)
				following = models.Follow.objects.filter(followedLog=respectiveLog, followerLog=usersLog).all()

				# CHECKING IF THE FOLLOW ALREADY EXISTS; IF IT DOES, ASSUME THIS AJAX REQUEST IS TO UNFOLLOW
				if following.count() > 0:
					following[0].delete()
				else:
					# NOT UNFOLLOWING
					following = models.Follow.objects.create(followedLog=respectiveLog, followerLog=request.user.roggeelog, approved=False)
					following.save()
					
				return render(request, 'rogging/null.html', {})
			else:
				return render(request, 'rogging/null.html', {})


def rogger404(request):
	return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))

def rogger500(request):
	return HttpResponseServerError(loader.render_to_string('rogging/500.html', context=RequestContext(request)))

def rogger403(request):
	return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))

def rogger400(request):
	return HttpResponseBadRequest(loader.render_to_string('rogging/400.html', context=RequestContext(request)))



def homepage(request):
	if not request.is_ajax():
		if request.user.is_authenticated():
		
			# GET THE LOGGED-IN USER'S RUNNING LOG
			respectiveLog = models.RoggeeLog.objects.get(user=request.user)
			
			# USE THAT RUNNING LOG TO GET THE USERS THAT THE USER FOLLOWS
			follows = models.Follow.objects.filter(followerLog=respectiveLog, followedLog__pseudoPrivate=True, approved=True) | models.Follow.objects.filter(followerLog=respectiveLog, followedLog__private=False, followedLog__pseudoPrivate=False)
			blocks = models.Block.objects.filter(blockerLog=request.user.roggeelog).values_list('blockedLog', flat=True)
			follows = follows.exclude(followedLog__in=blocks)
			follows = follows.order_by("followedLog__user__username").all()
			requests = None
			if (not respectiveLog.private) and respectiveLog.pseudoPrivate:
				requests = models.Follow.objects.filter(followedLog=respectiveLog, approved=False).order_by("followedLog__user__username").all()
			# HOLDS THE USERS FOLLOWED
			#followed = []
			
			# GOING THROUGH EACH FOLLOW TO GET EACH USER FOLLOWED
			#for follow in follows:
			#	if not respectiveLog.isBlockedByLog(follow.followedLog):
			#		followed.append(follow.followedLog)
		
			followed = follows.values_list('followedLog', flat=True)
		
			
			# Getting log entries
			logEntries = models.LogEntry.objects.filter(relatedLog__in=followed).order_by("-dateTimeCreated").all()
			# returning the first log entry that is used so that the fetchers on the front end know what log to fetch after when the front end wants more entries
			if logEntries.count() > 30:
				logEntries = logEntries[0:30]
		
			return render(request, 'rogging/homepage.html', { 'logEntries': logEntries, 'follows': follows, 'requests': requests })
		else:
			return render(request, 'rogging/homepage.html', {})
	else:
		if request.user.is_authenticated():
			# GETTING THE USER'S LOG
			respectiveLog = models.RoggeeLog.objects.get(user=request.user)
			ajaxType = request.POST['type'].encode('utf-8')
			if ajaxType == "updateEarlier":
				idOfRun = request.POST['idOfRun'].encode('utf-8')
				
				# HOLDS THE USERS THAT THE USER IS FOLLOWING
				usersFollowing = []
				
				# GET ALL OF THE USER'S FOLLOWS
				follows = models.Follow.objects.filter(followerLog=respectiveLog, approved=True) | models.Follow.objects.filter(followerLog=respectiveLog, followedLog__private=False, followedLog__pseudoPrivate=False)
				blocks = models.Block.objects.filter(blockerLog=request.user.roggeelog).values_list('blockedLog', flat=True)
				follows = follows.exclude(followerLog__in=blocks)
				follows = follows.order_by("followedLog__user__username").all()
				
				# ITERATE THROUGH ALL THE FOLLOWS, ADDING EACH FOLLOWED USER TO usersFollowing
				#for follow in follows:
				#	usersFollowing += [follow.followedLog]
				usersFollowing = follows.values_list('followedLog', flat=True)
				
				# THE WORKOUT AFTER WHICH TO GET THE EARLIER WORKOUTS
				originalRun = models.LogEntry.objects.get(id=idOfRun)
				
				# ALL WORKOUTS MADE BY THE USERS THAT THE USER FOLLOWS
				nextRuns = models.LogEntry.objects.filter(relatedLog__in=usersFollowing, dateTimeCreated__lt=originalRun.dateTimeCreated).order_by('-dateTimeCreated').all()
				
				# ONLY RETURN THE NEXT 30 RUNS IF THERE ARE MORE THAN 30
				if nextRuns.count() > 30:
					nextRuns = nextRuns[0:30]
				return render(request, 'rogging/update.html', { 'logEntries': nextRuns })
			if ajaxType == "followerApproval":
				if (not request.user.roggeelog.private) and request.user.roggeelog.pseudoPrivate:
					id = int(request.POST["userID"])
					user = User.objects.get(id=id)
					follow = models.Follow.objects.get(followerLog=user.roggeelog, followedLog=request.user.roggeelog)
					follow.approved = True
					follow.save()
					return render(request, 'rogging/null.html', {})
				else:
					return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))
			else:
				return HttpResponseBadRequest(loader.render_to_string('rogging/400.html', context=RequestContext(request)))
		else:
			return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))


def paceCalculator(hours, minutes, seconds, distance):
	totalSeconds = 3600*hours + 60*minutes + seconds
	paceInSeconds = totalSeconds/distance
	paceMinutes = int(paceInSeconds/60)
	paceSeconds = paceInSeconds%60
	
	secondsString = ""
	if paceSeconds < 10:
		secondsString = "0" + str(paceSeconds)
	else:
		secondsString = str(paceSeconds)
	
	return str(paceMinutes) + ":" + (secondsString[0:5] if len(secondsString) > 4 else secondsString)

def accessRun(request, run):
	# GET THE LOG ENTRY OF THE WORKOUT
	logEntry = get_object_or_404(models.LogEntry, id=run)
	
	if not logEntry.relatedLog.shouldRenderFor(request.user):#request.user.roggeelog.isBlockedByLog(logEntry.relatedLog):
		return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
	
	if not request.is_ajax():
		
		# GET THE LOG ENTRY'S USER
		respectiveUser = logEntry.relatedLog.user.username
		
		# GET THE LOG ENTRY'S RESPECTIVE LOG
		respectiveLog = logEntry.relatedLog
		
		blocks = []
		if request.user.is_authenticated():
			blocks = models.Block.objects.filter(blockerLog=request.user.roggeelog).values_list('blockedLog', flat=True)
		# GET THE COMMENTS BELONGING TO THE LOG ENTRY
		comments = models.Comment.objects.filter(respectiveRun=logEntry).exclude(commenterLog__in=blocks).order_by('dateTimeMade')
		
		# DECLARING THE FORM
		form = 0
		
		# DECLARING THE PACE
		pace = ""
		
		# GETTING THE DEFAULT UNITS + THE CUSTOM UNITS (METRICS)
		unitsInSortedOrder = getUnitsSorted(respectiveLog)
		
		# GETTING THE UNITS' CONVERSIONS (DISTANCE PER EACH UNIT)
		unitConversions = getConversions(respectiveLog)
		
		# IF A WORKOUT DURATION IS SPECIFIED, CALCULATE THE PACE
		if logEntry.hoursRan != None and logEntry.distance > 0:
			# GETTING THE PACE
			pace = paceCalculator(logEntry.hoursRan, logEntry.minutesRan, logEntry.secondsRan, logEntry.distanceInDesiredUnits())
		
		# IF THE USER CAN EDIT THIS ENTRY
		if request.method == "POST" and logEntry.relatedLog.user.username == request.user.username and request.user.is_authenticated():
			form = forms.RunViewForm(getChoicesForShoes(request.user.username), request.POST)
		
			if form.is_valid():
				
				# DID THE USER SPECIFY TO DELETE THE ENTRY? IF SO, DELETE IT AND REDIRECT
				if "delete" in request.POST:
					logEntry.delete()
					return HttpResponseRedirect(reverse('rogging:show', kwargs={ 'username': request.user.username }))
					
				# SET THE PROPERTIES OF THE LOG ENTRY BASED ON WHAT THE USER SUBMITTED
				logEntry.title = form.cleaned_data['title']
				logEntry.date = form.cleaned_data['date']
				logEntry.entry = form.cleaned_data['entry']
				logEntry.distance = form.cleaned_data['distance']
				logEntry.dateTimeCreated = datetime.datetime.now()
				
				# GET THE SHOE SPECIFIED (IF SPECIFIED) AND ASSIGN IT TO THE LOG ENTRY'S shoe PROPERTY
				if form.cleaned_data['shoe'] != "Select shoe":
					logEntry.shoe = models.Shoe.objects.get(name=form.cleaned_data['shoe'], relatedLog=models.RoggeeLog.objects.get(user=request.user))
				else:
					logEntry.shoe = None
				
				# IF THE DURATION OF THE WORKOUT IS SPECIFIED
				if form.cleaned_data['timeRan'] != '':
					timeRan = timeRanAutomaton(form.cleaned_data['timeRan'])
					
					# CALCULATING THE HOURS RAN (INCLUDING OVERFLOW FROM MINUTES IF MINUTES IS 60 OR OVER)
					logEntry.hoursRan = timeRan[0] + math.floor(timeRan[1] / 60)
					logEntry.minutesRan = timeRan[1] % 60
					logEntry.secondsRan = timeRan[2]
				else:
					logEntry.hoursRan = None
					logEntry.mintuesRan = None
					logEntry.secondsRan = None
					
				logEntry.kind = form.cleaned_data['type']
				logEntry.mervSubtype = form.cleaned_data['subtype'] if form.cleaned_data['subtype'] != "Select subtype" else None
				logEntry.mervDistanceUnit = "miles"
				logEntry.save()
				
				
				
				# TAG USERS
				tagEntry(logEntry)
				return render(request, 'rogging/loggereditsave.html', { 'respectiveLog': respectiveLog, 'comments': comments, 'useroflog': respectiveUser, 'id': run, 'form': form, 'pace': pace, 'units': unitsInSortedOrder, 'unitConversions': unitConversions, 'commentCount': logEntry.getNumberOfComments() })
			else:
				return render(request, 'rogging/loggeredit.html', { 'respectiveLog': respectiveLog, 'comments': comments, 'useroflog': respectiveUser, 'id': run, 'form': form, 'pace': pace, 'units': unitsInSortedOrder, 'unitConversions': unitConversions, 'commentCount': logEntry.getNumberOfComments() })
		else:
			# RETRIEVING THE LOG ENTRY DATA TO SHOW
			logEntry = models.LogEntry.objects.get(id=run)
			form = forms.RunViewForm(getChoicesForShoes(logEntry.relatedLog.user.username), initial={'shoe': logEntry.shoe.name if logEntry.shoe != None else "Select shoe", 'type': logEntry.kind, 'subtype': logEntry.mervSubtype if logEntry.mervSubtype != None else "Select subtype"})
			form.data['title'] = logEntry.title
			form.data['date'] = logEntry.date.isoformat()
			form.data['entry'] = logEntry.entry
			form.data['distance'] = logEntry.distanceInDesiredUnits()
			form.data['dateAdded'] = logEntry.dateTimeCreated
			form.data['shoe'] = logEntry.shoe.name if logEntry.shoe != None else "Select shoe"
			form.data['type'] = logEntry.kind
			form.data['subtype'] = logEntry.mervSubtype if logEntry.mervSubtype != None else "Select subtype"		
			
			
			# FORMATTING THE TIME CORRECTLY
			if logEntry.hoursRan != None:
				hours = str(int(logEntry.hoursRan))
				minutes = "0" + str(int(logEntry.minutesRan)) if logEntry.minutesRan < 10 else str(int(logEntry.minutesRan))
				seconds = "0" + str(logEntry.secondsRan) if logEntry.secondsRan < 10 else str(logEntry.secondsRan)

				form.data['timeRan'] = hours + ":" + minutes + ":" + seconds 
			else:
				form.data['timeRan'] = ''

			return render(request, 'rogging/loggeredit.html', { 'respectiveLog': respectiveLog, 'comments': comments, 'useroflog': respectiveUser, 'id': run, 'form': form, 'pace': pace, 'units': unitsInSortedOrder, 'unitConversions': unitConversions, 'commentCount': logEntry.getNumberOfComments() })
	else:
		type = request.POST['type'].encode('utf-8')
		if type == "comment":
			if request.user.is_authenticated() and not request.user.roggeelog.private:
				def possibleResponse():
					commenterRespectiveLog = models.RoggeeLog.objects.get(user=request.user)
					# GET THE POSTED COMMENT FROM THE POST DATA
					comment = request.POST['comment'].encode('utf-8')
					# IN-PLACE CREATION OF COMMENT
					comment = models.Comment.objects.create(dateTimeMade=datetime.datetime.now(), respectiveRun=logEntry, comment=comment, commenterLog=commenterRespectiveLog)
					comment.save()
			
					blocks = models.Block.objects.filter(blockerLog=request.user.roggeelog).values_list('blockedLog', flat=True)
					# GET THE COMMENTS BELONGING TO THE LOG ENTRY
					comments = models.Comment.objects.filter(respectiveRun=logEntry).exclude(commenterLog__in=blocks).order_by('dateTimeMade')
		
					# GETTING THE USERS THAT HAVE COMMENTED ON THIS WORKOUT
					usersInvolved = []
					for commentMade in comments:
						if commentMade.commenterLog.user.username not in usersInvolved and commentMade.commenterLog.user.username != comment.commenterLog.user.username:
							usersInvolved += [commentMade.commenterLog.user.username]
		
					# GETTING THE EMAILS OF THE USERS INVOLVED
					emailsInvolved = []
					for userInvolved in usersInvolved:
						emailsInvolved += [User.objects.get(username=userInvolved).email]
		
					# GETTING THE USER OF THE LOG ENTRY AND INVOLVE THEM IN THE EMAIL TO BE SENT OUT
					userOfEntry = logEntry.relatedLog.user
					if userOfEntry.email not in emailsInvolved and userOfEntry.username != comment.commenterLog.user.username:
						emailsInvolved += [userOfEntry.email]
		
					# SEND EMAIL
					commentContent = comment.comment.decode('utf-8')
					content = comment.commenterLog.user.username + " posted a comment on https://www.rogger.co/runs/" + run + " on " + str(logEntry.dateTimeCreated.year) + "-" + str(logEntry.dateTimeCreated.month) + "-" + str(logEntry.dateTimeCreated.day) + "\n\"" + commentContent + "\""
					sendEmail("Someone commented on a Rogger Post!", content, emailsInvolved)
					tagComment(comment, logEntry)
			
					return render(request, 'rogging/comment.html', { 'comments': comments })
				# THIS CALL MAY BE UNNECESSARY
				return logEntry.relatedLog.renderProperResponse(possibleResponse, request)
			else:
				return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))
		elif type == "deleteComment":
			id = request.POST['comment'].encode('utf-8')
			commenterRespectiveLog = models.RoggeeLog.objects.get(user=request.user)
			# GET COMMENT TO DELETE (FROM VARIABLE id ABOVE)
			comment = models.Comment.objects.get(id=id)
			
			# GET TAGS IN THE COMMENT
			tags = models.Tag.objects.filter(comment=comment).all()
			
			# IF THE USER IS THE USER OF THE COMMENT, DELETE THE COMMENT AND ITS RESPECTIVE TAGS
			if comment.commenterLog == commenterRespectiveLog and request.user.is_authenticated():
				# DELETE EACH TAG
				for tag in tags:
					tag.delete()
				comment.delete()
			
			blocks = models.Block.objects.filter(blockerLog=request.user.roggeelog).values_list('blockedLog', flat=True)
			# GET THE COMMENTS BELONGING TO THE LOG ENTRY
			comments = models.Comment.objects.filter(respectiveRun=logEntry).exclude(commenterLog__in=blocks).order_by('dateTimeMade')
			return render(request, 'rogging/comment.html', { 'comments': comments, 'id':run })
		else:
			return render(request, 'rogging/null.html', {})
def resources(request):
	return render(request, 'rogging/resources.html', {})




def getDaysInWeekFormat(fromDate):
	dayOfTheWeek = fromDate.isoweekday()
	week = [fromDate]
	i = 1
	preweek = []
	while i < dayOfTheWeek:
		workingDay = fromDate - datetime.timedelta(days=dayOfTheWeek-i)
		preweek += [workingDay]
		i += 1
	week = preweek + week
	postweek = []
	i = dayOfTheWeek + 1
	while i <= 7:
		workingDay = fromDate + datetime.timedelta(days=i-dayOfTheWeek)
		postweek += [workingDay]
		i += 1
	week = week + postweek
	return week
	
	



def importer(request):
	if request.user.is_authenticated():
		form = 0
		if request.is_ajax() == False:
		
			if request.method == "POST":
				form = forms.MervImportForm(request.POST)
			
				if form.is_valid():
					csvData = form.cleaned_data['mervCSVData']
					mervData = parseCSVrevamped.parseLogCSV(csvData)
					
					logOfUser = request.user.roggeelog
					for entry in mervData:
						shoe = 0
						if entry[8] != "":
							if models.Shoe.objects.filter(name=entry[8], relatedLog=logOfUser).all().count() == 0:
								shoe = models.Shoe.objects.create(name=entry[8], relatedLog=logOfUser)
								shoe.save()
							else:
								shoe = models.Shoe.objects.get(name=entry[8], relatedLog=logOfUser)
						else:
							shoe = None
						
						hours = None
						minutes = None
						seconds = None
						if entry[7] != "":
							floatTime = float(entry[7])
							hours = math.floor(floatTime/60)
							minutes = math.floor(floatTime%60)
							seconds = (floatTime - int(floatTime))*60
						
						yearmonthday = yearMonthDay(entry[0])
						now = datetime.datetime.now()
						pseudoDateTimeCreated = datetime.datetime(yearmonthday[0], yearmonthday[1], yearmonthday[2])
						importedEntry = models.LogEntry.objects.create(title=entry[10],
												date=entry[0],
												relatedLog=logOfUser,
												entry=entry[12],
												distance=float(entry[3]) if entry[3] != "" else 0,
												dateTimeCreated=(now if now < pseudoDateTimeCreated else pseudoDateTimeCreated),
												hoursRan=hours,
												minutesRan=minutes,
												secondsRan=seconds,
												shoe=shoe,
												kind=entry[1],
												mervSubtype=entry[2],
												mervAddendumToSummary=entry[11],
												mervRestHeartrate=float(entry[9]) if entry[9] != "" else 0,
												mervDistanceUnit=entry[4],
												mervPace=entry[5],
												mervPaceUnit=entry[6],
												imported=True)
						importedEntry.save()
					
					return HttpResponseRedirect(reverse('rogging:homepage'))
				else:
					return render(request, 'rogging/importerror.html', { 'form': form })
			else:
				form = forms.MervImportForm()
				return render(request, 'rogging/import.html', { 'form': form })
		else:
			return render(request, 'rogging/null.html', {})
	else:
		return HttpResponseRedirect(reverse('rogging:loginUser'))
		


def settings(request):
	if request.user.is_authenticated():
	
		# GETTING THE LOG OF THE USER
		respectiveLog = request.user.roggeelog
		form = 0
		
		if not request.is_ajax():
			
			# GET THE LOG'S SHOES
			shoes = models.Shoe.objects.filter(relatedLog=respectiveLog).all()
			
			# AN ARRAY THAT HOLDS ARRAYS OF SHOE/MILEAGE PAIRS
			shoesModified = []
			
			# GETTING THE METRICS THAT THE USER SPECIFIED
			metrics = models.Metric.objects.filter(relatedLog=respectiveLog).all()
			
			# ITERATING THROUGH ALL THE SHOES
			i = 0
			while i < len(shoes):
				
				milesOnShoe = 0
				
				# GET THE ENTRIES FOR THE WORKOUT WITH WHICH THE SHOE WAS USED
				entriesWithShoe = models.LogEntry.objects.filter(relatedLog=respectiveLog, shoe=shoes[i]).all()
				
				#ADD UP THE MILEAGE
				for entry in entriesWithShoe:
					milesOnShoe += entry.distanceInDesiredUnits()
				
				# ADD THE SHOE/MILEAGE PAIR TO shoesModified	
				shoesModified += [[shoes[i], milesOnShoe]]
				i += 1
			
			# IN-PLACE REASSIGNMENT
			shoes = shoesModified
			
			if request.method == 'POST':
				
				# FILLING THE FORM WITH THE POSTED DATA
				form = forms.SettingsForm(request.POST)
				
				if form.is_valid():
					# IF WE ARE ADDING A SHOE
					if "addShoe" in request.POST:
					
						# GETTING THE DESIRED SHOE NAME
						desiredShoe = form.cleaned_data['shoeName']
					
						# MAKING SURE THE NAME IS NOT EMPTY
						if len(desiredShoe) > 0:
							
							# IF THE SHOE ALREADY EXISTS, SHOW ERROR PAGE
							if models.Shoe.objects.filter(name=desiredShoe, relatedLog=respectiveLog).all().count() > 0:
								return render(request, 'rogging/settingserror.html', { 'form': form, 'shoes': shoes, 'metrics': metrics })
							else:
								# SAVING THE SHOE
								shoe = models.Shoe.objects.create(name=desiredShoe, relatedLog=respectiveLog)
								shoe.save()
								return HttpResponseRedirect(reverse('rogging:settings'))
						else:
							return HttpResponseRedirect(reverse('rogging:settings'))
					
					# ARE WE ADDING A METRIC?	
					if "addMetric" in request.POST:
					
						# GET THE DESIRED NAME OF THE METRIC
						metricName = form.cleaned_data['metricName'].encode('utf-8')
						
						# GET THE DISTANCE OF THE METRIC
						metricDistance = form.cleaned_data['metricDistance']
						
						# MAKING SURE THAT THE METRIC NAME IS NOT EMPTY
						if len(metricName) > 0:
							# MAKING SURE THE METRIC DOESN'T ALREADY EXIST
							if models.Metric.objects.filter(name=metricName, relatedLog=respectiveLog).all().count() > 0:
								return render(request, 'rogging/settingserror.html', {'form': form, 'shoes': shoes, 'metrics': metrics })
							else:
								# CREATE METRIC
								metric = models.Metric.objects.create(name=metricName, distance=metricDistance, relatedLog=respectiveLog)
								metric.save()
								
								return HttpResponseRedirect(reverse('rogging:settings'))
						else:
							return HttpResponseRedirect(reverse('rogging:settings'))
					elif "deleteImported" in request.POST:
						deleteImported(respectiveLog.user.username)
						return HttpResponseRedirect(reverse('rogging:settings'))
					else:
						return HttpResponseRedirect(reverse('rogging:settings'))
						
						
				else:
					return render(request, 'rogging/settingserror.html', { 'form': form, 'shoes': shoes, 'metrics': metrics })
			else:
				form = forms.SettingsForm()
				return render(request, 'rogging/settings.html', { 'form': form, 'shoes': shoes, 'metrics': metrics })
				
		else:
			type = request.POST['type'].encode('utf-8')
			if type == "deleteMetric":
				# GET ID OF METRIC TO BE DELETED
				id = request.POST['id'].encode('utf-8')
				# GET METRIC TO BE DELETED
				metric = get_object_or_404(models.Metric, id=id, relatedLog=respectiveLog) #KEEP RESPECTIVE LOG IN HERE FOR SECURITY REASONS
				metric.delete()
				# GET REMAINING METRICS TO DISPLAY ON THE FRONT END
				metrics = models.Metric.objects.filter(relatedLog=respectiveLog).all()
				return render(request, 'rogging/metrics.html', { 'metrics': metrics })
			elif type == "deleteShoe":
				# GET ID OF SHOE
				id = request.POST['id'].encode('utf-8')
				# GET SHOE TO BE DELETED
				shoe = get_object_or_404(models.Shoe, id=id, relatedLog=respectiveLog)
				# GET LOG ENTRIES THAT USE THIS SHOE AND DELETE THE SHOE FROM THE LOG ENTRY (BY SETTING IT TO None)
				logEntries = models.LogEntry.objects.filter(shoe=shoe)
				for entry in logEntries:
					entry.shoe = None
					entry.save()
				shoe.delete()
				# GET REMAINING SHOES TO DISPLAY ON THE FRONT END
				shoes = models.Shoe.objects.filter(relatedLog=respectiveLog).all()
				# PACKAGING SHOES WITH THEIR RESPECTIVE DISTANCE ON THEMSELVES
				shoesModified = []
				for i in xrange(len(shoes)):
					shoesModified += [[shoes[i], getDistanceOnShoe(shoes[i])]]
				return render(request, 'rogging/shoes.html', { 'shoes': shoesModified })
			elif type == "updateAccountSettings":
				password = request.POST['password']
				
				if authenticate(username=request.user.username, password=password) != None:
					form = forms.AccountSettingsForm(request.POST)
					
					if form.is_valid():
						if form.cleaned_data['email'] != "":
							request.user.email = form.cleaned_data['email']
						if form.cleaned_data['name'] != "":
							request.user.roggeelog.name = form.cleaned_data['name']
						privacy = form.cleaned_data['privacy']
						if privacy == "0":
							follows = models.Follow.objects.filter(followedLog=request.user.roggeelog)
							for follow in follows:
								follow.approved = True
								follow.save()
						if privacy == "1" and not (request.user.roggeelog.pseudoPrivate or request.user.roggeelog.private):
							follows = models.Follow.objects.filter(followedLog=request.user.roggeelog).exclude(followerLog=request.user.roggeelog)
							for follow in follows:
								follow.approved = False
								follow.save()
						if privacy == "2" and not (request.user.roggeelog.pseudoPrivate or request.user.roggeelog.private):
							follows = models.Follow.objects.filter(followedLog=request.user.roggeelog)
							for follow in follows:
								follow.approved = False
								follow.save()
						
						request.user.roggeelog.private = True if privacy == '2' else False
						request.user.roggeelog.pseudoPrivate = True if privacy == '1' else False
						request.user.roggeelog.searchName = True if form.cleaned_data['searchName'] == 'true' else False
						request.user.roggeelog.searchUsername = True if form.cleaned_data['searchUsername'] == 'true' else False
						request.user.roggeelog.loadUserPageFirst = True if form.cleaned_data['loadUserPageFirst'] == 'true' else False
						request.user.roggeelog.save()
						request.user.save()
						
						
						return JsonResponse({ 'email': request.user.email, 'name': request.user.roggeelog.name })
					else:
						return render(request, 'rogging/invalidnewemail.html', {})
				else:
					return render(request, 'rogging/wrongpassword.html', {})
			else:
				return render(request, 'rogging/null.html', {})
				
	else:
		return HttpResponseRedirect(reverse('rogging:loginUser'))	

def passwordReset(request):
	if not request.is_ajax():
		if request.method == "POST":
			form = forms.PWResetForm(request.POST)
			if form.is_valid():
				username = form.cleaned_data['username']
				potentialUsers = User.objects.filter(username=username)
				if potentialUsers.count() == 1:
					user = potentialUsers[0]
					email = user.email
					tokenGenerator = PasswordResetTokenGenerator()
					token = tokenGenerator.make_token(user)
					# from https://github.com/django/django/blob/master/django/contrib/auth/forms.py
					uid = utils.http.urlsafe_base64_encode(utils.encoding.force_bytes(user.pk))
					
					
					emailPlaintext = loader.render_to_string('rogging/passwordresetemail.txt', { 'user': user, 'token': token, 'uid': uid })
					emailHTML = loader.render_to_string('rogging/passwordresetemail.html', { 'user': user, 'token': token, 'uid': uid })
					
					message = EmailMultiAlternatives("Rogger Password Reset", emailPlaintext, "reset@rogger.co", [email])
					message.attach_alternative(emailHTML, 'text/html')
					message.send()
					
					return render(request, 'rogging/passwordresetsent.html', {})
				else:
					return render(request, 'rogging/passwordresetnosuchuser.html', { 'form': form })
			else:
				return render(request, 'rogging/passwordreseterror.html', { 'form': form })
		else:
			form = forms.PWResetForm()
			return render(request, 'rogging/passwordreset.html', { 'form': form })
	else:
		return render(request, 'rogging/null.html', {})
		
		
def passwordResetConfirm(request, uidb64, token):
	return views.password_reset_confirm(request, uidb64=uidb64, token=token, template_name='rogging/passwordresetconfirm.html', extra_context={ 'uid': uidb64, 'token': token })

def passwordResetComplete(request):
	return render(request, 'rogging/passwordresetcomplete.html', {})

def export(request):
	if not request.is_ajax():
		if request.user.is_authenticated():
			respectiveLog = models.RoggeeLog.objects.get(user=request.user)
			entries = models.LogEntry.objects.filter(relatedLog=respectiveLog)
			shoes = models.Shoe.objects.filter(relatedLog=respectiveLog)
			comments = models.Comment.objects.filter(commenterLog=respectiveLog)
			follows = models.Follow.objects.filter(followerLog=respectiveLog)
			tags = []
			sitewideTags = models.Tag.objects.all()
			for tag in sitewideTags:
				if (tag.entry != None and tag.entry.relatedLog == respectiveLog) or (tag.comment != None and tag.comment.commenterLog == respectiveLog):
					tags += [tag]
			metrics = models.Metric.objects.filter(relatedLog=respectiveLog)
			response = render(request, 'rogging/export.xml', { 'entries': entries, 'shoes': shoes, 'comments': comments, 'metrics': metrics, 'follows': follows, 'tags': tags }, content_type="text/xml")
			# from http://stackoverflow.com/questions/15790136/django-serving-a-download-file
			response['Content-Disposition'] = "attachment; filename=\"" + respectiveLog.user.username + "RoggerExport.xml\""
			return response
		else:
			return HttpResponseRedirect(reverse('rogging:loginUser'))
	else:
		return render(request, 'rogging/null.html', {})

def lostUsername(request):
	if not request.is_ajax():
		if request.method == "POST":
			form = forms.UsernamesRequestForm(request.POST)
			if form.is_valid():
				email = form.cleaned_data['emailAddress']
				users = User.objects.filter(email=email)
				if users.count() != 0:
					usersString = ""
					s = ""
					if users.count() == 1:
						usersString = users[0].username
					else:
						s = "s"
						for i in xrange(len(users)):
							if i == len(users) - 1:
								usersString += "and " + users[i].username
							else:
								if len(users) > 2:
									usersString += users[i].username + ", "
								else:
									usersString += users[i].username + " "
									
					
					emailPlaintext = loader.render_to_string('rogging/usernamesrequestemail.txt', { 's': s, 'usersString': usersString, 'email': email })
					emailHTML = loader.render_to_string('rogging/usernamesrequestemail.html', { 's': s, 'usersString': usersString, 'email': email })
					
					message = EmailMultiAlternatives("Rogger Username(s) Request", emailPlaintext, "support@rogger.co", [email])
					message.attach_alternative(emailHTML, 'text/html')
					message.send()
				else:
					emailPlaintext = loader.render_to_string('rogging/usernamesrequestemailnousernames.txt', {'email': email })
					emailHTML = loader.render_to_string('rogging/usernamesrequestemailnousernames.html', { 'email': email })
					
					message = EmailMultiAlternatives("Rogger Username(s) Request", emailPlaintext, "support@rogger.co", [email])
					message.attach_alternative(emailHTML, 'text/html')
					message.send()
					
				return render(request, 'rogging/usernamesemailsent.html', {})
			else:
				return render(request, 'rogging/lostusernamesinvalid.html', { 'form': form })
		else:
			form = forms.UsernamesRequestForm()
			return render(request, 'rogging/lostusernames.html', { 'form': form })
	else:
		return render(request, 'rogging/null.html', {})

def search(request):
	if not request.is_ajax():
		return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
	else:
		type = request.POST['type'].encode('utf-8')
		if type == "searchBar":
			if request.user.is_authenticated():
				query = request.POST['query'].encode('utf-8')
				results = userSearch(query, request.user)
				return render(request, 'rogging/searchresults.html', { 'results': results })
			else:
				return render(request, 'rogging/null.html', {})
		else:
			return render(request, 'rogging/null.html', {})
			
def accountSetup(request):
	if not request.is_ajax():
		return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
	else:
		type = request.POST['type'].encode('utf-8')
		if type == "initialAccountSetup":
			if request.user.is_authenticated():
				form = forms.AccountSetupForm(request.POST)
				if form.is_valid():
					relatedLog = request.user.roggeelog
					relatedLog.name = form.cleaned_data['name']
					relatedLog.searchUsername = True if form.cleaned_data['searchUsername'] == 'true' else False
					relatedLog.searchName = True if form.cleaned_data['searchName'] == 'true' else False
					relatedLog.save()
					return render(request, 'rogging/null.html', {})
				else:
					return render(request, 'rogging/ajaxerror.html', {})
			else:
				return render(request, 'rogging/null.html', {})
		else:
			return render(request, 'rogging/null.html', {})
			









































def deleteImported(user):
	# GET THE LOG OF THE USER FOR WHOM WE WANT TO DELETE THEIR IMPORTED LOG ENTRIES
	user = User.objects.get(username=user)
	respectiveLog = models.RoggeeLog.objects.get(user=user)
	
	# GET THEIR IMPORTED WORKOUTS
	importedRuns = models.LogEntry.objects.filter(relatedLog=respectiveLog, imported=True).all()
	
	# DELETE EACH WORKOUT
	for run in importedRuns:
		run.delete()
	
			
def getChoicesForShoes(user):
	# GET THE LOG OF THE USER
	user = User.objects.get(username=user)
	respectiveLog = models.RoggeeLog.objects.get(user=user)
	
	# GET THE SHOES OF THE LOG
	shoes = models.Shoe.objects.filter(relatedLog=respectiveLog)
	
	# SETTING UP THE INTIAL CHOICES FOR THE FRONT-END CHOOSER
	shoeChoices = [("Select shoe", "Select shoe")]
	
	# ADDING EACH SHOE AS CHOICE
	for shoe in shoes:
		shoeChoices += [(shoe.name, shoe.name)]
	
	return shoeChoices
	
		
def userAutomaton(entry):
	# AUTOMATON FOR FINDING TAGGED USERS
	# STATES ARE SELF-EXPLANATORY
	workingState = 0
	usersTagged = []
	numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
	letters = ["a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T", "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z"]
	currentUser = ""
	while (len(entry) > 0):
		if (workingState == 0):
			if (entry[0] == "@"):
				workingState = 1
				entry = entry[1:]
			else:
				workingState = 0
				entry = entry[1:]
		else:
			if (entry[0] in numbers or entry[0] in letters or entry[0] == "_"):
				currentUser = currentUser + entry[0]
				workingState = 1
				entry = entry[1:]
			else:
				if len(currentUser) > 0:
					usersTagged += [currentUser]
				workingState = 0
				entry = entry[1:]
				currentUser = ""
	
	if len(currentUser) > 0:
		usersTagged += [currentUser]
	return usersTagged
	
def tagEntry(logEntry):
	# Email users tagged
	# GET USERS TAGGED
	usersTagged = userAutomaton(logEntry.entry)
	
	# FOR EACH USER TAGGED
	for userTagged in usersTagged:
	
		# IF THE USER ACTUALLY EXISTS IN THE DATABASE
		if User.objects.filter(username=userTagged).all().count() > 0:
			
			# GETTING THE LOG OF THE USER TAGGED
			user = User.objects.get(username=userTagged)
			userTaggedLog = models.RoggeeLog.objects.get(user=user)
			
			# MAKE SURE TAG FOR USER WASN'T CREATED BEFORE (MAYBE THE USER WAS TAGGED BEFORE IN THE ENTRY)
			if models.Tag.objects.filter(entry=logEntry, comment=None, taggedLog=userTaggedLog).all().count() == 0:
				
				# CREATE TAG
				tag = models.Tag.objects.create(entry=logEntry, comment=None, taggedLog=userTaggedLog)
				
				if not logEntry.relatedLog.isBlockedByLog(userTaggedLog):
					# SEND EMAIL TO TAGGED USER TO NOTIFY THEM OF TAG
					sendEmail("You were tagged!", "You were tagged in a log entry at https://www.rogger.co/runs/" + str(logEntry.id), [user.email])
	
	# ALL THE TAGS THAT NO LONGER HAVE A CORRESPONDING TAG IN THE ENTRY SHOULD BE DELETED		
	for tag in models.Tag.objects.filter(entry=logEntry).all():
		if tag.taggedLog.user.username not in usersTagged:
			tag.delete()
			
def tagComment(comment, logEntry):
	# Email users tagged
	usersTagged = userAutomaton(comment.comment)
	
	# FOR EACH USER TAGGED
	for userTagged in usersTagged:
	
		# MAKE SURE THE USER ACTUALLY EXISTS BEFORE TAGGING THEM
		if User.objects.filter(username=userTagged).all().count() > 0:
		
			# GET THE LOG OF THE USER TAGGED
			user = User.objects.get(username=userTagged)
			userTaggedLog = models.RoggeeLog.objects.get(user=user)
			
			# MAKE SURE TAG FOR USER WASN'T CREATED BEFORE (MAYBE THE USER WAS TAGGED BEFORE IN THE COMMENT)
			if models.Tag.objects.filter(entry=None, comment=comment, taggedLog=userTaggedLog).all().count() == 0:
				
				# CREATE TAG
				tag = models.Tag.objects.create(entry=None, comment=comment, taggedLog=userTaggedLog)
				
				if not logEntry.relatedLog.isBlockedByLog(userTaggedLog):
					# EMAIL TAGGED USER TO NOTIFY THEM OF TAG
					sendEmail("You were tagged!", "You were tagged in a comment at https://www.rogger.co/runs/" + str(logEntry.id), [user.email])
	
	# ALL PREVIOUSLY CREATED TAGS THAT NO LONGER HAVE A CORRESPONDING TAG IN THE COMMENT SHOULD BE DELETED (THIS MAY NOT BE NECESSARY CODE)	
	for tag in models.Tag.objects.filter(comment=comment).all():
		if tag.taggedLog.user.username not in usersTagged:
			tag.delete()

def getDistanceOnShoe(shoe):
	# HOLDS THE TOTAL DISTANCE ON THE SHOE AS WE ADD IT UP
	distance = 0
	# GET LOG ENTRIES THAT USE THE SHOE
	logEntries = models.LogEntry.objects.filter(shoe=shoe)
	# ADD UP THE DISTANCES FOR THE LOG ENTRIES
	for entry in logEntries:
		distance += entry.distanceInDesiredUnits()
	return distance
	
def sendEmail(subject, content, recipients):
	for recipient in recipients:
		send_mail(subject, content, EMAILER, [recipient])
		sleep(0.1)
	
def getWeekOfWorkouts(date, respectiveLog):
	weekdays = { 0: 'Mon', 1: 'Tue', 2: 'Wed', 3: 'Thu', 4: 'Fri', 5: 'Sat', 6: 'Sun' }
	# GET WEEK FOR THAT EARLIER DAY
	week = getDaysInWeekFormat(date)
	# KEEPING TRACK OF WEEK'S DISTANCE
	distance = {'Run': 0, 'Bike': 0, 'Swim': 0, 'Weights': 0, 'Rower': 0, 'Stepper': 0, 'Skate': 0, 'Walk': 0, 'Ski': 0, 'Cross Train': 0}
		
	# FOR EACH DAY, ADD THE DAY'S RUNS' DISTANCES TO THE WEEKLY DISTANCE
	for i in xrange(len(week)):
		runs = models.LogEntry.objects.filter(relatedLog=respectiveLog, date=week[i]).all()
		for run in runs:
			distance[run.kind] += run.distanceInDesiredUnits()
		week[i] = [week[i]] + [runs] + [weekdays[i]] + [distance] 
	
	# PACKAGING THE DISTANCE WITH THE WEEK
	return [week] + [distance]
	
def makePDFfromHTML(html):
	# FROM http://agiliq.com/blog/2008/10/generating-pdfs-with-django/
	from xhtml2pdf import pisa
	fakeFile = StringIO()
	pdf = pisa.CreatePDF(html, fakeFile)
	fakeFile.seek(0)
	response = HttpResponse(fakeFile, content_type="application/pdf")
	return response
	

def getWeekPDF(request, username, weekof):
	if not request.is_ajax():
		if request.user.is_authenticated and request.user.username == username:
			if request.method == "POST":
				pdfForm = forms.WeekPDFForm(request.POST)
				if pdfForm.is_valid():
					response = makePDFfromHTML(pdfForm.cleaned_data['pdfData'])
					fileName = request.user.username + "weekof" + weekof + ".pdf"
					response["Content-Disposition"] = 'inline; filname=' + fileName
					return response
				else:
					return HttpResponseRedirect(reverse('rogging:homepage'))
			else:
				return HttpResponseRedirect(reverse('rogging:homepage'))
		else:
			return HttpResponseRedirect(reverse('rogging:homepage'))
	else:
		return render(request, "rogging/null.html", {})
	
	
def yearMonthDay(dateString):
	year = int(dateString[0:4])
	month = ""
	day = ""
	if dateString[6] == "-":
		month = int(dateString[5])
		if len(dateString) == 8:
			day = int(dateString[7])
		else:
			day = int(dateString[7:9])
	else:
		month = int(dateString[5:7])
		if len(dateString) == 9:
			day = int(dateString[8])
		else:
			day = int(dateString[8:10])
	
	return (year, month, day)
	
	