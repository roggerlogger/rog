from django.db import models
import datetime
from urlparse import urlparse, parse_qs
from time import time, clock
from views import awsImageRetrieval, getAWSInfo
from django.contrib.auth.models import User
from django.conf import settings
from django.http import Http404, HttpResponseForbidden, HttpResponseNotFound
from django.template.context import RequestContext
from django.template import loader
# Create your models here.

awsInfo = getAWSInfo()

class RoggeeLog(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, default=None)
	hasProfilePicture = models.BooleanField(default=False)
	hasBannerPicture = models.BooleanField(default=False)
	name = models.CharField(max_length=100, null=True, default=None)
	searchUsername = models.NullBooleanField(default=None)
	searchName = models.NullBooleanField(default=None)
	loadUserPageFirst = models.BooleanField(default=False)
	pseudoPrivate = models.BooleanField(default=False)
	private = models.BooleanField(default=False)
	
	def getThumbnail(self):
		global awsInfo
		return awsImageRetrieval(self.user, 'thumbnail', awsInfo)
	
	def getProfilePic(self):
		global awsInfo
		return awsImageRetrieval(self.user, 'profile', awsInfo)
	
	def getBannerPic(self):
		global awsInfo
		return awsImageRetrieval(self.user, 'banner', awsInfo)
	
	def showSetup(self):
		return (self.searchUsername == None
				or
				self.searchName == None
				or
				(self.name == None))
	
	def isBlockedByLog(self, blockerLog):
		return Block.objects.filter(blockedLog=self, blockerLog=blockerLog).exists()
	
	def isBlockedByUser(self, blocker):
		if blocker.is_authenticated():
			return Block.objects.filter(blockedLog=self, blockerLog=blocker.roggeelog).exists()
		else:
			return False
	
	def shouldRenderFor(self, requester):
		permissionToRender = True
		if (self.private or self.pseudoPrivate) and not requester.is_authenticated():
			permissionToRender = False
		if self.private and requester.is_authenticated() and self != requester.roggeelog:
			permissionToRender = False
		elif self.pseudoPrivate and requester.is_authenticated() and self != requester.roggeelog:
			if not Follow.objects.filter(followedLog=self, followerLog=requester.roggeelog, approved=True).exists():
				permissionToRender = False
		
		return permissionToRender
		
	def renderLevelForUser(self, user):
		if not user.is_authenticated():
			if not (self.private or self.pseudoPrivate):
				return 1
			else:
				if self.private:
					return -1
				else:
					return 0
		elif self != user.roggeelog and (self.private or user.roggeelog.isBlockedByLog(self)):
			return -1
		elif self.pseudoPrivate:
			if not (Follow.objects.filter(followedLog=self, followerLog=user.roggeelog, approved=True).exists() or self == user.roggeelog):
				return 0
		return 1
		
	def renderLevelFor(self, requestingLog):
		if requestingLog == None and not (self.private or self.pseudoPrivate):
			return 1
		if self != requestingLog and (self.private or requestingLog.isBlockedByLog(self)):
			return -1
		elif self.pseudoPrivate:
			if not (Follow.objects.filter(followedLog=self, followerLog=requestingLog, approved=True).exists() or self == requestingLog):
				return 0
		return 1
	
	def renderProperResponse(self, f, request):
		renderLevel = self.renderLevelForUser(request.user)
		if renderLevel == 1:
			return f()
		elif renderLevel == 0:
			return HttpResponseForbidden(loader.render_to_string('rogging/403.html', context=RequestContext(request)))
		else:
			return HttpResponseNotFound(loader.render_to_string('rogging/404.html', context=RequestContext(request)))
		
	def __str__(self):
		return self.user.username
		
class Shoe(models.Model):
	name = models.CharField(max_length=200)
	relatedLog = models.ForeignKey(RoggeeLog, related_name='shoe_relatedlog', null=True, default=None)
	
class LogEntry(models.Model):
	title = models.CharField(max_length=500)
	date = models.DateField()
	relatedLog = models.ForeignKey(RoggeeLog, related_name="logentry_relatedlog", null=True, default=None)
	entry = models.TextField()
	distance = models.FloatField(default=0.0)
	dateTimeCreated = models.DateTimeField(default=datetime.datetime.now())
	hoursRan = models.FloatField(default=0.0, null=True)
	minutesRan = models.FloatField(default=0.0, null=True)
	secondsRan = models.FloatField(default=0.0, null=True)
	shoe = models.ForeignKey(Shoe, related_name="logentry_shoe", default=None, null=True)
	kind = models.CharField(max_length=200, default="Run") #from merv
	
	# Merv/Import stuff
	mervSubtype = models.CharField(max_length=200, default=None, null=True) #from merv
	mervAddendumToSummary = models.CharField(max_length=500, null=True) #from merv
	mervRestHeartrate = models.FloatField(default=None, null=True) #from merv
	mervDistanceUnit = models.CharField(max_length=200, default="miles")
	mervPace = models.CharField(max_length=200, default=None, null=True)
	mervPaceUnit = models.CharField(max_length=200, default="/mi")
	imported = models.BooleanField(default=False)


	def __unicode__(self):
		return self.title
	
	def displayTitle(self):
		if self.title == "":
			return self.kind + ": " + str(self.distance) + " " + (self.mervAddendumToSummary if self.mervAddendumToSummary != None else "")
		else:
			return self.title + " " + (self.mervAddendumToSummary if self.mervAddendumToSummary != None else "")
	
	def distanceInDesiredUnits(self):
		# FOR NOW, THIS CONVERTS TO MILES
		if self.mervDistanceUnit == "miles" or self.mervDistanceUnit == "mi":
			return self.distance
		elif self.mervDistanceUnit == "km":
			return (self.distance * (1.0/1.609))
		else:
			return (self.distance * (1.0/1609.0))
			
	def getNumberOfComments(self):
		return Comment.objects.filter(respectiveRun=self).count()
		
	def timeRanString(self):
		if self.hoursRan != None and self.minutesRan != None and self.secondsRan != None:
			hoursFormatted = str(int(self.hoursRan)) if self.hoursRan >= 10 else "0" + str(int(self.hoursRan))
			minutesFormatted = str(int(self.minutesRan)) if self.minutesRan >= 10 else "0" + str(int(self.minutesRan))
			secondsFormatted = str(self.secondsRan) if self.secondsRan >= 10 else "0" + str(self.secondsRan)
			return hoursFormatted + ":" + minutesFormatted + ":" + secondsFormatted
		return ""
	
	def paceString(self):
		if self.hoursRan != None and self.minutesRan != None and self.secondsRan != None and self.distanceInDesiredUnits() > 0.0:
			from views import paceCalculator
			return paceCalculator(self.hoursRan, self.minutesRan, self.secondsRan, self.distanceInDesiredUnits())
		return ""
		
		

class Comment(models.Model):
	dateTimeMade = models.DateTimeField(default=datetime.datetime.now())
	respectiveRun = models.ForeignKey(LogEntry, related_name='comment_respectiverun')
	comment = models.TextField()
	commenterLog = models.ForeignKey(RoggeeLog, related_name='comment_commenterlog', null=True, default=None)
	
	def __str__(self):
		return "Comment by " + self.commenterLog.user.username
	
class Follow(models.Model):
	followedLog = models.ForeignKey(RoggeeLog, related_name='follow_followedlog', null=True, default=None)
	followerLog = models.ForeignKey(RoggeeLog, related_name='follow_followerlog', null=True, default=None)
	approved = models.BooleanField(default=False)
	
class Block(models.Model):
	blockedLog = models.ForeignKey(RoggeeLog, related_name='block_blockedlog')
	blockerLog = models.ForeignKey(RoggeeLog, related_name='block_blockerlog')
	
class Tag(models.Model):
	entry = models.ForeignKey(LogEntry, related_name='tag_entry', null=True)
	comment = models.ForeignKey(Comment, related_name='tag_comment', null=True)
	taggedLog = models.ForeignKey(RoggeeLog, related_name='tag_taggedlog', null=True, default=None)
	
class Metric(models.Model):
	relatedLog = models.ForeignKey(RoggeeLog, related_name='metric_relatedlog', null=True, default=None)
	name = models.CharField(max_length=200)
	distance = models.FloatField(default=0.0)
	
class PictureCacheURL(models.Model):
	relatedLog = models.ForeignKey(RoggeeLog, related_name='picturecacheurl_relatedlog', null=True, default=None)
	url = models.CharField(max_length=1000)
	forResource = models.CharField(max_length=100)
	
	def stillValid(self):
		parsedURL = urlparse(self.url)
		parsedQuery = parse_qs(parsedURL.query)
		validUntil = parsedQuery['Expires'][0]
		buffer = 20 #in seconds
		if (time() + 20) < int(validUntil):
			return True
		else:
			return False
	
	def __str__(self):
		parsedURL = urlparse(self.url)
		parsedQuery = parse_qs(parsedURL.query)
		validUntil = parsedQuery['Expires'][0]
		return self.relatedLog.user.username + ", expires " + str(datetime.datetime.utcfromtimestamp(float(validUntil))) + " UTC"



