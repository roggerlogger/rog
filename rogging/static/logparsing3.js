// OVERALL EXPLANATION (the writing of which was suggested by Daniel Chazin): THIS FILE CONTAINS METHODS THAT PARSE A LOG ENTRY OF A RUNNER, WHICH WOULD LOOK
// SOMETHING LIKE "I ran +4miles in the morning and +4miles in the evening, finishing the second run with +4x200meter intervals". THE "+" SYMBOL, SIMILAR TO
// TWITTER'S "#", DENOTES A RUN. THE AUTOMATON IN THIS FILE PARSES A LOG ENTRY, LOOKING FOR THESE RUNS AND ORGANIZING EACH RUN INTO AN ARRAY, AND PUTTING
// EVERY ONE OF THOSE ARRAYS INTO AN ENCAPSULATING ARRAY. THE AUTOMATON ALSO KEEPS TRACK OF THE POSITIONS, IN THE LOG ENTRY, OF THESE RUNS SO THAT SOMEONE
// CAN FORMAT THE LOG ENTRY, USING THE POSITION DATA, SO THAT THE RUNS STAND OUT.


var units = ["miles", "mile", "mi.", "mi", "meters", "meter", "m", "kilometers", "kilometer", "km", "k"];
var metersThreshold = 49;

// ASSISTING FUNCTIONS FOR AUTOMATON BELOW

// DETERMINES IF THE NEXT PHRASE IN THE LOG IS A UNIT
function isUnit(log, units) {
	for (i = 0; i < units.length; i++) {
		var placeFound = log.indexOf(units[i]);
		if (placeFound == 0) {
			return true;
		}
	}
	return false;
}

// DETERMINES THE LENGTH, IN NUBMER OF CHARACTERS, OF THE NEXT UNIT IN THE LOG
function unitLength(log, units) {
	for (i = 0; i < units.length; i++) {
		var placeFound = log.indexOf(units[i]);
		if (placeFound == 0) {
			return units[i].length;
		}
	}
	return 0;
}



// AUTOMATON FOR PARSING LOG ENTRIES
// AUTOMATON IDEA FROM http://nedbatchelder.com/text/python-parsers.html

function logParsingAutomaton(log, units) {
	// THIS CONTAINS ALL OF THE subOutputs THAT ARE CREATED FROM THE PARSING, SEE BELOW
	var finalOutput = [];
	// KEEPS TRACK OF THE POSITIONS OF ALL THE RUNS IN THE LOG ENTRY SO THAT A DEVELOPER KNOWS
	// WHERE TO FORMAT THE TEXT ASSOCIATED WITH A RUN
	var positionOutput = [];
	// subOutput CONTAINS THE PARTS OF A RUN (LIKE "+5miles"), AND GETS APPENDED TO finalOutput
	// WHICH CONTAINS ALL OF THE PARSED RUNS IN THE LOG ENTRY
	var subOutput = [""];
	var workingState = 0;
	var numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
	var letters = ["a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T", "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z"];
	var initialLogLength = log.length;
	var x = 0;

	var startPosition = 0;
	var endPosition = 0;
	while (x < initialLogLength + 1) {
		// STATE ZERO
		if (workingState == 0) {
			//console.log("Entering State 0");
			// FOUND A PLUS SIGN
			if (log[0] == "+") {
				workingState = 1;
				log = log.slice(1, log.length);
				startPosition = x;
				x = x + 1;
				continue;
			} else {
				workingState = 0;
				log = log.slice(1, log.length);
				x = x + 1;
				continue;
			}
		
		
		// STATE ONE
		} else if (workingState == 1) {
			//console.log("Entering State 1");
			// FOUND A NUMBER
			if (numbers.indexOf(log[0]) >= 0) {
				subOutput[subOutput.length-1] = subOutput[subOutput.length-1] + log[0];
				log = log.slice(1, log.length);
				workingState = 2;
				x = x + 1;
				continue;
			// FOUND A UNIT
			} else if (isUnit(log, units)) {
				var unit = [log.slice(0, unitLength(log, units))];
				subOutput = subOutput.concat(unit);
				log = log.slice(unitLength(log, units), log.length);
				workingState = 6;
				x = x + unit[0].length;
				continue;
			// THE LOG HAS HIT THE END, STOP THE AUTOMATON
			} else if (log.length == 0) {
				break;
			} else {
				subOutput = [""];
				workingState = 0;
				continue;
			}


		// STATE TWO
		} else if (workingState == 2) {
			//console.log("Entering State 2");
			if (numbers.indexOf(log[0]) >= 0) {
				// FOUND A NUMBER
				subOutput[subOutput.length-1] = subOutput[subOutput.length-1] + log[0];
				log = log.slice(1, log.length);
				workingState = 2;
				x = x + 1;
				continue;
			} else if (log[0] == "x" || log[0] == "X") {
				// FOUND AN "x" or "X" THAT MAY REPRESENT "by"
				log = log.slice(1, log.length);
				workingState = 5;
				x = x + 1;
				continue;
			} else if (log.slice(0, 2) == "by" || log.slice(0, 2) == "BY") {
				// FOUND "by" OR "BY"
				log = log.slice(2, log.length);
				workingState = 5;
				x = x + 2;
				continue;
			} else if (log[0] == ".") {
				// FOUND A POTENTIAL DECIMAL POINT
				subOutput[subOutput.length-1] = subOutput[subOutput.length-1] + ".";
				log = log.slice(1, log.length);
				workingState = 3;
				x = x + 1;
				continue;
			} else if (isUnit(log, units)) {
				// FOUND A UNIT
				unit = [log.slice(0, unitLength(log, units))];
				subOutput = subOutput.concat(unit);
				log = log.slice(unitLength(log, units), log.length);
				workingState = 6;
				x = x + unit[0].length;
				continue;
			} else {
				workingState = 6;
				continue;
			}


		// STATE THREE
		} else if (workingState == 3) {
			//console.log("Entering State 3")
			if (numbers.indexOf(log[0]) >= 0) {
				// FOUND A NUMBER
				subOutput[subOutput.length-1] = subOutput[subOutput.length-1] + log[0];
				log = log.slice(1, log.length);
				workingState = 4;
				x = x + 1;
				continue;
			} else {
				workingState = 6;
				continue;
			}


		// STATE FOUR
		} else if (workingState == 4) {
			//console.log("Entering State 4")
			if (numbers.indexOf(log[0]) >= 0) {
				// FOUND A NUMBER
				subOutput[subOutput.length-1] = subOutput[subOutput.length-1] + log[0];
				log = log.slice(1, log.length);
				workingState = 2;
				x = x + 1;
				continue;
			} else if (log[0] == "x" || log[0] == "X") {
				// FOUND AN "x" OR "X" THAT MAY REPRESENT "by"
				log = log.slice(1, log.length);
				workingState = 5;
				x = x + 1;
				continue;
			} else if (log.slice(0, 2) == "by" || log.slice(0, 2) == "BY") {
				// FOUND A "by" OR "BY"
				log = log.slice(2, log.length);
				workingState = 5;
				x = x + 2;
				continue;
			} else if (isUnit(log, units)) {
				// FOUND A UNIT
				unit = [log.slice(0, unitLength(log, units))];
				subOutput = subOutput.concat(unit);
				log = log.slice(unitLength(log, units), log.length);
				workingState = 6;
				x = x + unit[0].length;
				continue;
			} else {
				workingState = 6;
				continue;
			}


		// STATE FIVE
		} else if (workingState == 5) {
			//console.log("Entering State 5");
			if (numbers.indexOf(log[0]) >= 0) {
				// FOUND A NUMBER
				subOutput = subOutput.concat([""]);
				subOutput[subOutput.length-1] = subOutput[subOutput.length-1] + log[0];
				log = log.slice(1, log.length);
				workingState = 2;
				x = x + 1;
				continue;
			} else if (isUnit(log, units)) {
				// FOUND A UNIT
				unit = [log.slice(0, unitLength(log, units))];
				subOutput = subOutput.concat(unit);
				log = log.slice(unitLength(log, units), log.length);
				workingState = 6;
				x = x + unit[0].length;
				continue;
			} else {
				workingState = 6;
				continue;
			}
		

		// STATE SIX
		} else if (workingState == 6) {
			//console.log("Entering State 6");
			finalOutput = finalOutput.concat([subOutput]);
			
			// IF THERE IS MORE LOG LEFT, GO BACK TO STATE ZERO AND PARSE IT
			if (log.length > 0) {
				workingState = 0;
				subOutput = [""];
				endPosition = x;
				positionOutput = positionOutput.concat([[startPosition, endPosition]]);
				continue;
			} else {
				// FOUND NO MORE LOG LEFT, FINISH UP THINGS BEFORE RETURNING
				endPosition = x;
				positionOutput = positionOutput.concat([[startPosition, endPosition]]);
				break;
			}
		}
	}
	return [finalOutput, positionOutput];
}


//console.log("Final Result");
//console.log(logParsingAutomaton("today +5.5miles +2km +1 +mile +4.9by2.2kilometers"));


var defaultUnit = "mile";
var kmConv;
var mileConv;
var meterConv;

// THIS SECTION CONVERTS EVERY UNIT INTO HOW MUCH THE DEFAULT UNIT SPECIFIED ABOVE IT REPRESENTS, 
// SO IF THE DEFAULT UNIT IS A MILE, THEN A KILOMETER REPRESENTS 0.6125 OF A MILE
if (defaultUnit == "mile") {
	kmConv = 0.6215;
	mileConv = 1.0;
	meterConv = 0.0006215;
} else {
	kmConv = 1.0;
	mileConv = 1.609;
	meterConv = 0.001;
}

var unitLengths = {"kilometers": kmConv, "kilometer": kmConv, "km": kmConv, "k": kmConv, "miles": mileConv, "mile": mileConv, "mi.": mileConv, "mi": mileConv, "meters": meterConv, "meter": meterConv, "m": meterConv};

// THIS TAKES THE LIST OF RUNS GENERATED BY THE AUTOMATON ABOVE AND CALCULATES THE TOTAL DISTANCE RAN ACCOUNTED FOR IN THE LOG ENTRY
function addRuns(runs, units, unitLengths) {
	var milesRan = 0;
	for (i = 0; i < runs.length; i++) {
		var runTotal = 0;
		var noUnits = false;
		// IF WE FIND A UNIT IN ONE RUN, MAKE RUN TOTAL THAT UNIT'S DEFAULT LENGTH
		if (units.indexOf(runs[i][runs[i].length-1]) != -1) {
			runTotal = unitLengths[runs[i][runs[i].length-1]];
		} else {
			runTotal = 1;
			noUnits = true;
		}
		
		// ADDING ALL THE PARTS OF THE RUN TOGETHER, SO "+4x5x2miles" GETS CONVERTED TO 4*5*2 miles = 40 miles
		for (j = 0; j < (noUnits ? runs[i].length : runs[i].length-1); j++) {
			if (runs[i][0] != "") {
				// DETERMINING IF, IF THERE ARE NO UNITS SPECIFIED BUT A NUMBER IS, WHETHER OR NOT IT SHOULD BE CONSIDERED TO
				// BE IN THE DEFAULT UNIT OR METERS (AS IN "Ran 4" VERSUS "Ran 400", WHERE THE FORMER MAY BE INTENDED TO MEAN
				// THAT THE PERSON RAN FOUR IN THE DEFAULT UNIT, WHILE THE LATTER MAY BE INTENDED TO MEAN THAT THE PERSON RAN
				// 400 METERS). THIS IS DETERMINED BY SEEING IF THE NUMBER CROSSES A NUMERIC THRESHOLD CALLED
				// "metersThreshold"
				if (noUnits && (j+1) == runs[i].length && parseFloat(runs[i][j]) > metersThreshold) {
					runTotal = runTotal * (parseFloat(runs[i][j])/(1/unitLengths["meters"]));
				} else {
					runTotal = runTotal * parseFloat(runs[i][j]);
				}
			}
		}

		
		milesRan = milesRan + runTotal;
		
	}
	
	return milesRan;
}

// TESTS
//var timingStartDate = new Date();
//var beginning = timingStartDate.getTime();
//console.log(addRuns(logParsingAutomaton("today +5.5miles and +2km and +1 and +mile and +4.9by2.2kilometers done")[0]));
//var timingEndDate = new Date();
//console.log(timingEndDate.getTime() - beginning);
