# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('rogging', '0004_auto_20150408_1541'),
    ]

    operations = [
        migrations.AddField(
            model_name='logentry',
            name='hoursRan',
            field=models.FloatField(default=0.0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='logentry',
            name='minutesRan',
            field=models.FloatField(default=0.0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='logentry',
            name='secondsRan',
            field=models.FloatField(default=0.0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='logentry',
            name='dateTimeCreated',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 8, 16, 55, 53, 522769)),
        ),
    ]
