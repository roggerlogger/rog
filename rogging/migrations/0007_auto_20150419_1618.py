# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('rogging', '0006_auto_20150408_1703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logentry',
            name='dateTimeCreated',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 19, 16, 18, 46, 328510)),
        ),
        migrations.AlterField(
            model_name='logentry',
            name='hoursRan',
            field=models.FloatField(default=0.0, null=True),
        ),
        migrations.AlterField(
            model_name='logentry',
            name='minutesRan',
            field=models.FloatField(default=0.0, null=True),
        ),
        migrations.AlterField(
            model_name='logentry',
            name='secondsRan',
            field=models.FloatField(default=0.0, null=True),
        ),
    ]
