# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('rogging', '0002_logentry_distance'),
    ]

    operations = [
        migrations.AddField(
            model_name='logentry',
            name='dateTimeCreated',
            field=models.DateTimeField(default=datetime.date(2015, 4, 8)),
            preserve_default=False,
        ),
    ]
