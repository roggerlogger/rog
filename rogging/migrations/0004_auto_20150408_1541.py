# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('rogging', '0003_logentry_datetimecreated'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logentry',
            name='dateTimeCreated',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 8, 15, 41, 3, 896761)),
        ),
    ]
