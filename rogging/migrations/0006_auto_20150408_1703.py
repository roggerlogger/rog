# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('rogging', '0005_auto_20150408_1655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logentry',
            name='dateTimeCreated',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 8, 17, 3, 26, 565008)),
        ),
    ]
