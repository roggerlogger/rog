from django.conf.urls import url

import views
from django.contrib import auth

urlpatterns = [
	url(r'^login$', views.loginUser, name='loginUser'),
	url(r'^createaccount$', views.createAccount, name='createAccount'),
	url(r'^accountcreated$', views.accountCreated, name='accountCreated'),
	url(r'^logoutsuccess$', views.logoutSuccess, name='logoutSuccess'),
	url(r'^logger$', views.roggerLogger, name='roggerLogger'),
	url(r'^resources$', views.resources, name='resources'),
	url(r'^importer$', views.importer, name='importer'),
	url(r'^settings$', views.settings, name='settings'),
	url(r'^settings/changepassword$', views.changePassword, name='changePassword'),
	url(r'^settings/export$', views.export, name='export'),
	url(r'^lostusernames$', views.lostUsername, name='lostUsername'),
	url(r'^search$', views.search, name='search'),
	url(r'^accountsetup$', views.accountSetup, name='accountSetup'),
	url(r'^(?P<username>([0-9]|[a-z]|[A-Z]|_)+)$', views.show, name='show'),
	url(r'^runs/(?P<run>[0-9]+)$', views.accessRun, name='accessRun'),
	url(r'^(?P<username>([0-9]|[a-z]|[A-Z]|_)+)/weekof(?P<weekof>([0-9][0-9][0-9][0-9][0-1][0-9][0-3][0-9])).pdf$', views.getWeekPDF, name='getPDF'),
	url(r'^$', views.homepage, name='homepage')
]
