import models
import datetime

def importDateConversion():
	logEntries = models.LogEntry.objects.filter(imported=True)
	for logEntry in logEntries:
		entryDate = logEntry.date
		logEntry.dateTimeCreated = datetime.datetime(entryDate.year, entryDate.month, entryDate.day)
		logEntry.save()

def pushDownFuturePosts():
	logEntries = models.LogEntry.objects.filter(imported=True)
	now = datetime.datetime.now()
	
	for logEntry in logEntries:
		comparisonTime = datetime.datetime(logEntry.date.year, logEntry.date.month, logEntry.date.day)
		if now < comparisonTime:
			logEntry.dateTimeCreated = now
			logEntry.save()	