import boto3
from boto.s3.connection import S3Connection
from imageprocessing import preparePicture
from django.contrib.auth.models import User
from StringIO import StringIO
from rog.settings import IS_CONNECTED, AWS_PICTURE_ID, AWS_PICTURE_SECRET, PICTURE_BUCKET_NAME
from rogging.models import RunningLog, Comment, RoggeeLog, Shoe, LogEntry, Comment, Follow, Tag, Metric, PictureCacheURL

def makeThumbnails():
	users = User.objects.all()
	for user in users:
		session = None
		client = None
		s3 = None
		bucket = None
		connection = None
		boto2bucket = None
			
		connection = S3Connection(AWS_PICTURE_ID, AWS_PICTURE_SECRET)
		boto2bucket = connection.get_bucket(PICTURE_BUCKET_NAME)
		session = boto3.session.Session(AWS_PICTURE_ID, AWS_PICTURE_SECRET)
		client = session.client('s3')
		s3 = session.resource('s3')
		bucket = s3.Bucket(PICTURE_BUCKET_NAME)
		if boto2bucket.get_key(str(user.id) + 'profpic.jpg') != None:
			originalPictureStream = StringIO(client.get_object(Bucket=PICTURE_BUCKET_NAME, Key=str(user.id)+"profpic.jpg")['Body'].read())
			profileThumbnailStream = preparePicture(originalPictureStream, (100, 100))
			bucket.put_object(Key=str(user.id)+'profthumb.jpg', Body=profileThumbnailStream.getvalue())
			
def defineHasProfilePicture():
	users = User.objects.all()
	
	for user in users:
		connection = S3Connection(AWS_PICTURE_ID, AWS_PICTURE_SECRET)
		boto2bucket = connection.get_bucket(PICTURE_BUCKET_NAME)
		
		if boto2bucket.get_key(str(user.id) + 'profpic.jpg') != None:
			runningLog = RunningLog.objects.get(user=user.username)
			runningLog.hasProfilePicture = True
			runningLog.save()

def repairComments():
	comments = Comment.objects.all()
	
	for comment in comments:
		respectiveLog = RunningLog.objects.get(user=comment.user)
		comment.userRespectiveLog = respectiveLog
		comment.save()

def repairRunningLogs():
	logs = RunningLog.objects.all()
	
	for log in logs:
		print log.user
		user = User.objects.get(username=log.user)
		log.usr = user
		log.save()


def replaceRunningLogs():
	logs = RunningLog.objects.all()
	
	for log in logs:
		newLog = RoggeeLog.objects.create(user=log.usr, hasProfilePicture=log.hasProfilePicture)
		newLog.save()
		
		shoes = Shoe.objects.filter(log=log)
		for shoe in shoes:
			shoe.relatedLog = newLog
			shoe.save()
			
		entries = LogEntry.objects.filter(respectiveLog=log)
		for entry in entries:
			entry.relatedLog = newLog
			entry.save()
			
		comments = Comment.objects.filter(userRespectiveLog=log)
		for comment in comments:
			comment.commenterLog = newLog
			comment.save()
		
		follows = Follow.objects.filter(userFollowed=log)
		for follow in follows:
			follow.followedLog = newLog
			follow.save()
		follows = Follow.objects.filter(userFollowing=log)
		for follow in follows:
			follow.followerLog = newLog
			follow.save()
			
		tags = Tag.objects.filter(userTagged=log)
		for tag in tags:
			tag.taggedLog = newLog
			tag.save()
		
		metrics = Metric.objects.filter(respectiveLog=log)
		for metric in metrics:
			metric.relatedLog = newLog
			metric.save()
			
		urls = PictureCacheURL.objects.filter(respectiveLog=log)
		for url in urls:
			url.relatedLog = newLog
			url.save()
		
		
			
		
		



































		
		
		
		
		
		