from django.conf.urls import patterns, include, url, handler404, handler500, handler403, handler400
from django.contrib import admin, auth
from rogging import views
import settings
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin', include(admin.site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^passwordreset$', views.passwordReset, name='passwordReset'),
	# part past "passwordresetconfirm/" from https://docs.djangoproject.com/en/1.9/topics/auth/default/#using-the-views
	url(r'^passwordresetconfirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$', views.passwordResetConfirm, name='password_reset_confirm'),
	url(r'^passwordresetcomplete$', views.passwordResetComplete, name='password_reset_complete'),
    url(r'^', include('rogging.urls', namespace="rogging")),
)

handler404 = 'rogging.views.rogger404'
handler500 = 'rogging.views.rogger500'
handler403 = 'rogging.views.rogger403'
handler400 = 'rogging.views.rogger400'
